export class User {

    constructor (
        public name: string,
        public lastname: string,
        public email: string,
        public role: string,
        public zone: string[],
        public company: string[],
        public password?: string,
        public client?: string,
        public avatar?: string,
        public status?: boolean,
        public user?: string,
        public _id?: string,
        public __v?: string
    ) {}
}
