export class Zone {

    constructor (
        public name: string,
        public status?: boolean,
        public _id?: string,
        public __v?: string
    ) {}
}
