export class Delivery {

    constructor (
        public company: string,
        public price: number,
    ) { }

}
