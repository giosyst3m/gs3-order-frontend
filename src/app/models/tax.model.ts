export class Tax {

    constructor (
        public name: string,
        public tax: number,
        public from: Date,
        public to: Date,
        public status?: Boolean,
        public _id?: string,
        public __v?: string
    ) { }

}
