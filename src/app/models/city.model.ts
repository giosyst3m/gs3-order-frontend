export class City {

    constructor (
        public name: string,
        public state: string,
        public status?: boolean,
        public _id?: string,
        public __v?: string
    ) {}
}
