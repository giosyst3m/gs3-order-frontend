export class Log {

    constructor (
        public code: string,
        public message: string,
        public entity: string,
        public id: string,
        public user: string,
        public type: string,
        public status?: boolean,
        public _id?: string,
        public __v?: string
    ) {}
}
