export class Counter {

    constructor (
        public name: String,
        public number: Number,
        public company: String,
        public status?: Boolean,
        public _id?: String,
        public __v?: String
    ) {}
}
