export class Product {

    constructor (
        public sku?: String,
        public barcode?: String,
        public type?: String,
        public name?: String,
        public line?: String,
        public brand?: String,
        public conditon?: String,
        public price?: Number,
        public status?: Boolean,
        public category?: String,
        public code?: String,
        public sticker_sell?: String,
        public sticker_color?: String,
        public stock?: Number,
        public date?: Date,
        public img?: String,
        public img_url?: String,
        public __v?: Number,
        public _id?: string,
    ) { }

}
