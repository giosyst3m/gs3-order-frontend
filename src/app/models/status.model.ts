export class Status {

    constructor (
        public name: string,
        public order: number,
        public icon: string,
        public color: string,
        public next: string,
        public message: string,
        public type: string,
        public role: string[],
        public addProduct: string[],
        public edit?: boolean,
        public delivery?: boolean,
        public deleteDetailInZero?: boolean,
        public deleteNoAssigned?: boolean,
        public billAssociated?: boolean,
        public billShow?: boolean,
        public billForm?: boolean,
        public deleteDetailNoInventory?: boolean,
        public applyDiscount?: boolean,
        public validBill?: boolean,
        public status?: boolean,
        public showStock?: boolean,
        public buttom?: string[],
        public note?: string,
        public _id?: string,
        public __v?: string,
    ) { }

}