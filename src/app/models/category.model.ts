export class Category {

    constructor (
        public name: string,
        public code: number,
        public status?: boolean,
        public _id?: string,
        public __v?: string
    ) {}
}
