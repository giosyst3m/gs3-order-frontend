export class Document {

    constructor (
        public client: string,
        public company: string,
        public tax: string[],
        public type?: string,
        public number?: string,
        public note?: string,
        public delivery?: string,
        public discount?: string,
        public status?: string,
        public date?: Date,
        public pay_date?: Date,
        public pay_total?: number,
        public user?: string,
        public _id?: string,
        public __v?: string
    ) { }

}
