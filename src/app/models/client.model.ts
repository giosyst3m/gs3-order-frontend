export class Client {

    constructor (
        public code: String,
        public correlative: String,
        public name: String,
        public address: String,
        public city: String,
        public zone: String[],
        public status?: Boolean,
        public mobil?: String,
        public phone?: String,
        public _id?: string,
        public __v?: Number
    ) { }

}
