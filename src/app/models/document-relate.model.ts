export class DocumentRelate {

    constructor (
        public number: string,
        public price: number,
        public status: boolean,
        public date: Date,
        public _id?: string
    ) { }

}
