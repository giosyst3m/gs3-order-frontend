export class Inventory {

    constructor (
        public product: string,
        public quantity: number,
        public company: string,
        public type: boolean,
        public origin: string,
        public document: string,
        public status: string,
        public required?: number,
        public detail?: string,
        public date?: Date,
        public _id?: string,
        public __v?: Number
    ) { }

}
