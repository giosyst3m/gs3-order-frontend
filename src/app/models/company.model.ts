export class Company {

    constructor (
        public coode: string,
        public name: string,
        public address: string,
        public status: boolean,
        public phone: string,
        public email: string,
        public web?: string,
        public logo?: string,
        public _id?: string,
        public __v?: Number
    ) { }

}
