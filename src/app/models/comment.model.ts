export class CommentGrupo {

    constructor (
        public message: string,
        public type?: string,
        public row?: string,
        public status?: Number,
        public _id?: string,
        public date?: Date,
        public __v?: Number
    ) { }

}
