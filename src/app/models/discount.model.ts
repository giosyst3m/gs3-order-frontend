export class Discount {

    constructor (
        public name: string,
        public discount: number,
        public status: number,
        public _id?: string,
        public __v?: Number
    ) { }

}