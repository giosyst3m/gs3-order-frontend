export class Detail {

    constructor (
        public document: string,
        public product: string,
        public quantity: number,
        public origin: string,
        public price?: number,
        public request?: string,
        public discount?: string,
        public note?: string,
        public status?: number,
        public stock?: number,
        public _id?: string,
        public __v?: number
    ) { }

}
