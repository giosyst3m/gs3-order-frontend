export class Access {

    constructor (
        public role: string,
        public resource: string,
        public action: string,
        public attributes: string,
        public status?: boolean,
        public _id?: string,
        public __v?: string
    ) {}
}
