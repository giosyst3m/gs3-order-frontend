import { Component, OnInit, OnDestroy } from '@angular/core';
import { SidebarService, EntityService } from '../../services/service.index';
import { Subject } from 'rxjs/Subject';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit, OnDestroy {

  public menu: any = [];
  destroySubject$: Subject<void> = new Subject();

  constructor(
    private _sidebar: SidebarService,
    private _EntityS: EntityService
  ) {

    this._EntityS.getAll('menu')
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((resp: any) => {
        this.menu = resp.data;
      });
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    this.destroySubject$.next();
    this.destroySubject$.complete();
  }
}
