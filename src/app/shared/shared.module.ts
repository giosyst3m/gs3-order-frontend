// Cloudinary
import { Cloudinary } from 'cloudinary-core';
import { CloudinaryModule, CloudinaryConfiguration } from '@cloudinary/angular-5.x';

import { PipesModule } from '../pipes/pipes.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { BreadcrumsComponent } from './breadcrums/breadcrums.component';
import { FooterComponent } from './footer/footer.component';
import { ProfileMenuComponent } from './profile-menu/profile-menu.component';
import { FooterButtonsComponent } from './footer-buttons/footer-buttons.component';
import { ModalUploadComponent } from './modal-upload/modal-upload.component';
import { CartComponent } from './cart/cart.component';
import { CommentGroupComponent } from './comment-group/comment-group.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DocumentIMPComponent } from './document-imp/document-imp.component';
import { SearchClientComponent } from './search/search-client.component';
import { DocumentPDFComponent } from './document-pdf/document-pdf.component';
import { LogComponent } from './log/log.component';
import { SearchClientIdComponent } from './search/search-client-id.component';

@NgModule({
    declarations: [
        HeaderComponent,
        SidebarComponent,
        BreadcrumsComponent,
        FooterComponent,
        FooterButtonsComponent,
        ProfileMenuComponent,
        ModalUploadComponent,
        CartComponent,
        CommentGroupComponent,
        DocumentIMPComponent,
        SearchClientComponent,
        DocumentPDFComponent,
        LogComponent,
        SearchClientIdComponent
    ],
    exports: [
        HeaderComponent,
        SidebarComponent,
        BreadcrumsComponent,
        FooterComponent,
        FooterButtonsComponent,
        ProfileMenuComponent,
        ModalUploadComponent,
        CommentGroupComponent,
        DocumentIMPComponent,
        SearchClientComponent,
        DocumentPDFComponent,
        LogComponent,
        SearchClientIdComponent
    ],
    imports: [
        RouterModule,
        CommonModule,
        PipesModule,
        FormsModule,
        ReactiveFormsModule,
        CloudinaryModule.forRoot({Cloudinary}, { cloud_name: 'giosyst3m' } as CloudinaryConfiguration),
    ]
})

export class SharedModule { }
