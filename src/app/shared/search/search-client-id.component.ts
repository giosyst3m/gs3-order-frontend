import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { takeUntil } from 'rxjs/operators';
import { ClientService } from './../../services/service.index';
import { Client } from './../../models/index.models';


@Component({
  selector: 'app-search-client-id',
  templateUrl: './search-client-id.component.html',
  styleUrls: ['./search-client-id.component.css']
})
export class SearchClientIdComponent implements OnInit, OnDestroy {
  destroySubject$: Subject<void> = new Subject();
  @Input() id: string;
  @Input() isClient: boolean = false;
  client: Client;

  constructor(
    private _ClientService: ClientService
  ) { }

  ngOnInit() {
    this._ClientService.getById(this.id)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((resp: any) => {
        this.client = resp.data;
      })
  }
  ngOnDestroy(): void {
    this.destroySubject$.next();
    this.destroySubject$.complete();
  }
}
