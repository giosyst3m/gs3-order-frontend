import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { takeUntil } from 'rxjs/operators';
import { Client } from '../../models/index.models';
import { ClientService } from '../../services/service.index';

@Component({
  selector: 'app-search-client',
  templateUrl: './search-client.component.html',
  styleUrls: ['./search-client.component.css']
})
export class SearchClientComponent implements OnInit, OnDestroy {
  destroySubject$: Subject<void> = new Subject();
  @Output() clientVerify: EventEmitter<string> = new EventEmitter<string>();

  clients: Client[];
  client: string;
  clientTotalRecords: Number = 0;
  clientLoading = false;
  clientData = '';
  constructor(
    private _ClientService: ClientService
  ) { }

  ngOnInit() {
  }

  searchingClients(keywords) {
    this.clientLoading = true;
    this._ClientService.getAll(0, keywords)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((data: any) => {
        this.clients = data.data;
        this.clientTotalRecords = data.total;
        this.clientLoading = false;
      });

  }

  verify(id: string) {
    this.clientData = id;
    this.clientVerify.emit(id);
  }
  ngOnDestroy(): void {
    this.destroySubject$.next();
    this.destroySubject$.complete();
  }
}
