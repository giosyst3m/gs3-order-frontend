import { UserService } from './../../services/service.index';
import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/index.models';

@Component({
  selector: 'app-profile-menu',
  templateUrl: './profile-menu.component.html',
  styleUrls: ['./profile-menu.component.css']
})
export class ProfileMenuComponent implements OnInit {
  user: User;
  constructor(
    private _UserService: UserService
  ) {
    this.user = this._UserService.getUser();
  }

  ngOnInit() {
  }

}
