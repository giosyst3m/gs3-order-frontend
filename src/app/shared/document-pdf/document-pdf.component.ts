import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { takeUntil } from 'rxjs/operators';
import { DocumentService, CompanyService, PNotifyService } from '../../services/service.index';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-document-pdf',
  templateUrl: './document-pdf.component.html',
  styleUrls: ['./document-pdf.component.css']
})
export class DocumentPDFComponent implements OnInit, OnDestroy {
  destroySubject$: Subject<void> = new Subject();
  @Input() document: string;
  email: string;
  img = false;
  company: string;
  loading = false;
  link = false;
  url: string;
  pnotify = this._pnotify.getPNotify();

  constructor(
    private _DocumentService: DocumentService,
    private _CompanyService: CompanyService,
    private _pnotify: PNotifyService,
  ) {
    this.company = this._CompanyService.getCompanySelect();
  }

  ngOnInit() {
  }

  generatePDF(form: NgForm) {
    this.loading = true;
    this.link = false;
    this.url = '';
    if (form.value.email) {
      this.pnotify.info({
        title: 'Generando Archivo PDF.',
        text: `Ud recibirá un email con el link de descarga ${form.value.email}`
      });
    }
    this._DocumentService.getPDFByID(this.document, form.value)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((resp: any) => {
        this.loading = false;
        if (resp.data.file.url) {
          this.url = resp.data.file.url;
          this.link = true;
        }
      }, (err) => {
        this.pnotify.error({
          title: 'Error',
          text: 'No se pudo guardar la información, intentar mas tarde ' + err
        });
      });
  }
  ngOnDestroy(): void {
    this.destroySubject$.next();
    this.destroySubject$.complete();
  }
}
