import { ModalUploadService, PNotifyService, SubirArchivoService} from '../../services/service.index';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-modal-upload',
  templateUrl: './modal-upload.component.html',
  styles: []
})
export class ModalUploadComponent implements OnInit {

  imagenSubir: File;
  imagenTemp: string;
  pnotify = this._pnotify.getPNotify();
  btnSubir = false;

  constructor(
    public _subirArchivoService: SubirArchivoService,
    public _modalUploadService: ModalUploadService,
    private _pnotify: PNotifyService,
  ) {}

  ngOnInit() {
  }

  cerrarModal() {
    this.imagenTemp = null;
    this.imagenSubir = null;
    this._modalUploadService.ocultarModal();
  }

  seleccionImage( archivo: File ) {

    if ( !archivo ) {
      this.imagenSubir = null;
      return;
    }

    if ( archivo.type.indexOf('image') < 0 ) {
      this.pnotify.error({
        title: 'Tipo Archivo Incorrecto',
        text: `El archivo Seleccionado ${ archivo.name } no es permitidp, favor subir otro`
      });
      this.imagenSubir = null;
      return;
    }
    this.btnSubir = true;
    this.imagenSubir = archivo;

    let reader = new FileReader();
    let urlImagenTemp = reader.readAsDataURL( archivo );

    reader.onloadend = () => this.imagenTemp = reader.result;

  }

  subirImagen() {

    this._subirArchivoService.subirArchivo( this.imagenSubir, this._modalUploadService.tipo, this._modalUploadService.id )
          .then( (resp: any) => {
              this.pnotify.success({
              title: 'Fin de procesos',
              text: resp.message
            });
            this._modalUploadService.notificacion.emit( resp )
            this.cerrarModal();

          })
          .catch( err => {
            this.pnotify.error({
              title: 'Error al susbir la imagen',
              text: err
            });
          });

  }

}
