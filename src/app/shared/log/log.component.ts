import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { takeUntil } from 'rxjs/operators';
import { Log } from '../../models/index.models';
import { LogService } from '../../services/service.index';

@Component({
  selector: 'app-log',
  templateUrl: './log.component.html',
  styleUrls: ['./log.component.css']
})
export class LogComponent implements OnInit, OnDestroy {
  destroySubject$: Subject<void> = new Subject();
  logs: Log[] = [];
  @Input() id: string;
  @Input() entity: string;
  totalRecords: Number = 0;
  from = 0;

  constructor(
    private _logService: LogService
  ) {}

  ngOnInit() {
    this.getAll();
  }
  getAll() {
    this._logService.getByEntityID(this.entity, this.id, this.from )
    .pipe(takeUntil(this.destroySubject$))
    .subscribe((resp: any) => {
      this.logs = resp.data;
      this.totalRecords = resp.total;
    });
  }

  chageFrom(value) {
    const from = this.from + value;
    if ( from >= this.totalRecords ) {
      return;
    }
    if ( from < 0 ) {
      return;
    }

    this.from += value;
    this.getAll();
   }
   ngOnDestroy(): void {
    this.destroySubject$.next();
    this.destroySubject$.complete();
  }
}
