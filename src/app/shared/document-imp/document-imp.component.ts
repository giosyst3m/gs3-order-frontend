import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { takeUntil } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { DocumentService } from '../../services/service.index';
import { Document, Detail, Tax, Delivery, Discount, Inventory } from '../../models/index.models';

@Component({
  selector: 'app-document-imp',
  templateUrl: './document-imp.component.html',
  styleUrls: ['./document-imp.component.css']
})
export class DocumentIMPComponent implements OnInit, OnDestroy {
  destroySubject$: Subject<void> = new Subject();
  document: Document;
  details: Detail[];
  taxes: Tax[];
  deliveries: Delivery[];
  discounts: Discount;
  id: string;
  totals: any;
  des: Discount[];
  totalRows: number;
  from = 0;
  limit = 50;
  lodaing = true;
  inventory: Inventory;
  todate: any;

  constructor(
    public activatedRoute: ActivatedRoute,
    private _DocumentS: DocumentService,
  ) {

    activatedRoute.params.subscribe(params => {
      this.id = params['id'];
      this.getDocument();
      this.getAll();
    });
    this.todate = new Date();

  }

  getDocument() {
    this._DocumentS.getById(this.id)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((resp: any) => {
        this.document = resp['data'];
        this.taxes = resp['data']['tax'];
        this.deliveries = resp['data']['delivery'];
        this.discounts = resp['data']['discount'];
        this.getTotals();
      });
  }

  ngOnInit() {
  }
  getTotals() {
    this._DocumentS.getTotalsById(this.id)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((resp: any) => {
        this.totals = resp['data'];
      });
    setTimeout(() => {
      window.print();
    }, 3000);
  }

  getAll() {
    this.lodaing = true;
    this.getDetail();
  }

  getDetail() {
    this._DocumentS.getDetail(this.id, this.from, this.limit)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((resp) => {
        this.details = resp['data'];
        this.totalRows = resp['total'];
        this.lodaing = false;
      });
  }
  ngOnDestroy(): void {
    this.destroySubject$.next();
    this.destroySubject$.complete();
  }
}
