import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { takeUntil } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PNotifyService, EntityService, CommentService } from '../../services/service.index';
import { CommentGrupo } from '../../models/index.models';

@Component({
  selector: 'app-comment-group',
  templateUrl: './comment-group.component.html',
  styleUrls: ['./comment-group.component.css']
})
export class CommentGroupComponent implements OnInit, OnDestroy {
  destroySubject$: Subject<void> = new Subject();
  model: CommentGrupo = new CommentGrupo(null);
  pnotify = this._pnotify.getPNotify();
  form: FormGroup;
  commentsGroup: CommentGrupo[];
  from = 0;
  totalRecords: Number = 0;
  @Input() row: string;
  @Input() type: string;

  constructor(
    private _ES: EntityService,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    private _pnotify: PNotifyService,
    private _CommentS: CommentService
  ) {

  }


  save() {
    if (this.form.invalid) {
      return;
    }

    this._ES.save('comment', this.form.value)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe(resp => {
        if (resp.result) {
          this.pnotify.success({
            title: 'Conformación',
            text: 'Comentario Agregado '
          });
          this.form.reset({
            _id: null,
            message: null,
            type: this.type,
            row: this.row,
            status: true,
            __v: null,
          });
          this.getAll();
        } else {
          this.pnotify.error({
            title: 'Error',
            text: 'No se pudo guardar la información, intentar mas tarde '
          });
        }
      });
  }
  ngOnInit() {
    this.form = new FormGroup({
      _id: new FormControl(),
      message: new FormControl('', Validators.required),
      type: new FormControl(this.type, Validators.required),
      row: new FormControl(this.row, Validators.required),
      status: new FormControl(true),
      __v: new FormControl(),
    });
    this.getAll();
  }

  getAll() {
    this._CommentS.getAll(this.type, this.row, this.from)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((resp: any) => {
        this.commentsGroup = resp.data;
        this.totalRecords = resp.total;
      });
  }

  chageFrom(value) {
    const from = this.from + value;
    if (from >= this.totalRecords) {
      return;
    }

    if (from < 0) {
      return;
    }

    this.from += value;
    this.getAll();
  }
  ngOnDestroy(): void {
    this.destroySubject$.next();
    this.destroySubject$.complete();
  }
}
