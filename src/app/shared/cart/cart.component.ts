import { CartService } from '../../services/service.index';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'Rxjs/Subscription';
@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit, OnDestroy {
  dataPassed: any;
  total: number;
  document: string;
  subscription: Subscription;
  constructor(
    private _CartS: CartService
  ) {

      // subscribe to home component messages
    this.subscription = this._CartS.getData()
      .subscribe((x: any) => {
        if ( x ) {
          this.dataPassed = x.data;
          this.total = x.total;
          this.document = x.document;
        }
    });

   }

  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.subscription.unsubscribe();
  }
  ngOnInit() {
  }

}
