import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { takeUntil } from 'rxjs/operators';
import { EntityService, UserService } from '../../services/service.index';
import { User } from '../../models/user.model';



@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {
  destroySubject$: Subject<void> = new Subject();
  companies: any[];
  user: User;

  constructor(
    private _ES: EntityService,
    public _UserService: UserService
  ) {

  this._ES.getAll('company')
    .pipe(takeUntil(this.destroySubject$))
    .subscribe((data: any) => {
      this.companies = data.data;
    });

    this.user = this._UserService.getUser();
  }

  ngOnInit() {

  }
  ngOnDestroy(): void {
    this.destroySubject$.next();
    this.destroySubject$.complete();
  }

}
