import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'status'
})
export class StatusPipe implements PipeTransform {

  transform(status: boolean): String {
    if ( status ) {
      return 'Activo';
    } else {
      return 'Inactivo';
    }
  }

}
