import { NgModule } from '@angular/core';
import { StatusPipe } from './status.pipe';
import { KeepHTMLPipe } from './keep-html.pipe';

@NgModule({
  imports: [],
  declarations: [
    StatusPipe,
    KeepHTMLPipe
  ],
  exports: [
    StatusPipe,
    KeepHTMLPipe
  ]
})
export class PipesModule { }
