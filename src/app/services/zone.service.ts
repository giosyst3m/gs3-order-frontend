import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/Http';
import 'rxjs/add/operator/map';


@Injectable({
  providedIn: 'root'
})
export class ZoneService {

  constructor(
    private http: HttpClient
  ) { }

  getZoneSelect() {
    let company = document.getElementById('zoneSelect');
    for (let index = 0; index < company.childElementCount; index++) {
      const element = company.children[index];
      if (element['selected']) {
        return element['value'];
      }
    }
  }

}
