import { environment } from './../../environments/environment.prod';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/Http';
import 'rxjs/add/operator/map';
import { UserService } from './service.index';

@Injectable()
export class TaxService {
  path = `${environment.URL_SERVICES}/tax`;

  constructor(
    private http: HttpClient,
    private _UserService: UserService
  ) { }

  getTaxCurrent() {
    return this.http.get(`${this.path}/current`, { params: { token: this._UserService.getToken(), access: 'read:any', table: 'tax' } });
  }
}
