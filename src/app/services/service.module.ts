import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  SidebarService,
  PNotifyService,
  EntityService,
  ModalUploadService,
  SubirArchivoService,
  ProductService,
  DocumentService,
  CompanyService,
  TaxService,
  ErrorService,
  CartService,
  InventoryService,
  DocumentRelateService,
  DashboardService,
  UserService,
  LogService
 } from './service.index';





@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
    SidebarService,
    PNotifyService,
    EntityService,
    ModalUploadService,
    SubirArchivoService,
    ProductService,
    DocumentService,
    CompanyService,
    TaxService,
    ErrorService,
    CartService,
    InventoryService,
    DocumentRelateService,
    DashboardService,
    UserService,
    LogService
  ],
  declarations: []
})
export class ServiceModule { }
