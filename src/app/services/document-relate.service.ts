import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/Http';
import 'rxjs/add/operator/map';
import { UserService } from './service.index';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DocumentRelateService {

  constructor(
    private http: HttpClient,
    private _UserService: UserService
  ) { }

  save(document: string, model: any) {
    let url = environment.URL_SERVICES + '/document/relate/' + document;

    return this.http.post(url, model, { params: { token: this._UserService.getToken(), access: 'create:any', table: 'documentrelate' } })
      .map((resp: any) => {
        return resp;
      });

  }

  getAllByDocument(document: string, from: number = 0, limit: number = 5) {
    let url = environment.URL_SERVICES + '/document/relate/' + document + '?from=' + from + '&limit=' + limit;
    return this.http.get(url, { params: { token: this._UserService.getToken(), access: 'read:any', table: 'documentrelate' } })
      .map((resp: any) => resp);
  }
}
