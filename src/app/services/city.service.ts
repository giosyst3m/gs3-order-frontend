import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/Http';
import 'rxjs/add/operator/map';
import { UserService } from './service.index';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CityService {

  constructor(
    private http: HttpClient,
    private _UserService: UserService
  ) { }

  getAll(from: number = 0, value: string = '') {
    let url = environment.URL_SERVICES + '/city?from=' + from + '&value=' + value;
    return this.http.get(url, { params: { token: this._UserService.getToken(), access: 'read:any', table: 'city' } })
      .map((resp: any) => resp);
  }

  getById(id: string) {

    let url = environment.URL_SERVICES + '/city/' + id;
    return this.http.get(url, { params: { token: this._UserService.getToken(), access: 'read:any', table: 'city' } })
      .map((resp: any) => resp.data);

  }

  save(model: any) {
    let url = environment.URL_SERVICES + '/city';

    if (model._id) {// Update

      url += '/' + model._id;

      return this.http.put(url, model, { params: { token: this._UserService.getToken(), access: 'update:any', table: 'city' } })
        .map((resp: any) => {
          return resp;

        });

    } else { // Create
      return this.http.post(url, model, { params: { token: this._UserService.getToken(), access: 'create:any', table: 'city' } })
        .map((resp: any) => {
          return resp;
        });
    }
  }

  active(id: string) {
    let url = environment.URL_SERVICES + '/city/active/' + id;

    return this.http.patch(url, { status: true }, { params: { token: this._UserService.getToken(), access: 'detete:any', table: 'city' } })
      .map((resp: any) => resp.data);

  }

  inactive(id: string) {
    let url = environment.URL_SERVICES + '/city/inactive/' + id;

    return this.http.patch(url, { status: false }, { params: { token: this._UserService.getToken(), access: 'delete:any', table: 'city' } })
      .map((resp: any) => resp.data);

  }
}
