import { environment } from './../../../environments/environment';

import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { HttpClient } from '@angular/common/Http';
import { UserService } from '../service.index';


@Injectable()
export class CartService {
    private subject = new Subject<any>();

    constructor(
        private http: HttpClient,
        private _UserService: UserService
    ) { }

    sendData(message: any) {
        this.subject.next(message);
    }

    clearData() {
        this.subject.next();
    }

    getData(): Observable<any> {
        return this.subject.asObservable();
    }

    getAll(id) {
        return this.http.get(`${environment.URL_SERVICES}/document/detail/${id}`, { params: { token: this._UserService.getToken(), access: 'read:any', table: 'document' } });
    }

}
