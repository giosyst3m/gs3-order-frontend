import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/Http';
import 'rxjs/add/operator/map';
import { UserService } from '../service.index';



@Injectable({
  providedIn: 'root'
})
export class ModelService {
  path = `${environment.URL_SERVICES}/entity`;

  constructor(
    private http: HttpClient,
    private _UserService: UserService
  ) { }

  getAll(table: string, from: number = 0, value: string = '') {
    return this.http.get(`${this.path}/${table}?from=${from}&value=${value}`, { params: { token: this._UserService.getToken(), access: 'read:any' } })
      .map((resp: any) => resp);
  }

  getById(table: string, id: string) {

    return this.http.get(`${this.path}/${table}/${id}`, { params: { token: this._UserService.getToken(), access: 'read:any' } })
      .map((resp: any) => resp.data);
  }

  save(table: string, model: any) {

    if (model._id) {// Update

      return this.http.put(`${this.path}/${table}/${model._id}`, model, { params: { token: this._UserService.getToken(), access: 'update:any' } })
        .map((resp: any) => {
          return resp;

        });

    } else { // Create
      return this.http.post(`${this.path}`, model, { params: { token: this._UserService.getToken(), access: 'create:any' } })
        .map((resp: any) => {
          return resp;
        });
    }
  }

  active(table: string, id: string) {
    return this.http.patch(`${this.path}/active/${table}/${id}`, { status: true }, { params: { token: this._UserService.getToken(), access: 'delete:any' } })
      .map((resp: any) => resp.data);
  }

  inactive(table: string, id: string) {
    return this.http.patch(`${this.path}/inactive/${table}/${id}`, { status: false }, { params: { token: this._UserService.getToken(), access: 'delete:any' } })
      .map((resp: any) => resp.data);

  }

  delete(table: string, id: string) {
    return this.http.delete(`${this.path}/${table}/${id}`, { params: { token: this._UserService.getToken(), access: 'delete:any' } });
  }
}
