import { environment } from './../../../environments/environment';
import { UserService } from './../service.index';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/Http';
import { RECORDS_LIMIT } from '../../config/config';
import 'rxjs/add/operator/map';

@Injectable()
export class EntityService {
  path = `${environment.URL_SERVICES}/entity`;

  constructor(
    private http: HttpClient,
    private _UserService: UserService
  ) { }

  /**
   * Get List records
   * @param table string name of entity
   * @param from number from record wants to start
   * @param value string value to search
   * @param limit nubmer how many records wants to get Defoutl define by RECORDS_LIMIT
   * @param status number Get Records 1 = Active 2 = Inactive 3 = both
   * @return object {Status, Result, Data, TotalRecords, [error]}
   */
  getAll(table: string, from: number = 0, value: string = '', limit: number = RECORDS_LIMIT, status: number = 1) {
    return this.http.get(`${this.path}/${table}?from=${from}&limit=${limit}&value=${value}&status=${status}`, { params: { token: this._UserService.getToken(), access: 'read:any' } })
      .map((resp: any) => resp);
  }

  /**
   * Get Record from entity by Id
   * @param table string name of entity
   * @param id strig of entity
   * @return object {Status, Result, Data, [error]}
   */
  getById(table: string, id: string) {

    return this.http.get(`${this.path}/${table}/${id}`, { params: { token: this._UserService.getToken(), access: 'read:any' } })
      .map((resp: any) => resp.data);
  }

  /**
   * Create a record from Entity
   * Update a record from Entity by Id
   * @param table string name of entity
   * @param model array data to save.
   * @return object {Status, Result, Data, [error]}
   */
  save(table: string, model: any) {

    if (model._id) {// Update

      return this.http.put(`${this.path}/${table}/${model._id}`, model, { params: { token: this._UserService.getToken(), access: 'update:any' } })
        .map((resp: any) => {
          return resp;

        });

    } else { // Create
      return this.http.post(`${this.path}/${table}`, model, { params: { token: this._UserService.getToken(), access: 'create:any' } })
        .map((resp: any) => {
          return resp;
        });
    }
  }

  /**
   * Active a record from entity by id
   * @param table string name entity
   * @param id string id record
   * @return object {Status, Resutl, data, [error]}
   */
  active(table: string, id: string) {
    return this.http.patch(`${this.path}/active/${table}/${id}`, { status: true }, { params: { token: this._UserService.getToken(), access: 'update:any' } })
      .map((resp: any) => resp);
  }

  /**
   * Inactive a record from entity by id
   * @param table string name entity
   * @param id string id record
   * @return object {Status, Resutl, data, [error]}
   */
  inactive(table: string, id: string) {
    return this.http.patch(`${this.path}/inactive/${table}/${id}`, { status: false }, { params: { token: this._UserService.getToken(), access: 'update:any' } })
      .map((resp: any) => resp);

  }

  /**
   * Delete a record by id
   * @param table string name of Entity
   * @param id strign id of Etity
   * @return object {Status, Resutl, data, [error]}
   */
  delete(table: string, id: string) {
    return this.http.delete(`${this.path}/${table}/${id}`, { params: { token: this._UserService.getToken(), access: 'delete:any' } });
  }

}
