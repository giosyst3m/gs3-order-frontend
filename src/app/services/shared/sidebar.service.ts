import { Injectable } from '@angular/core';

@Injectable()
export class SidebarService {

menu: any = [
{
  name: 'Catalogo',
  icon: 'fa fa-shopping-cart',
  url: '#',
  submenu: [{
    name: 'Productos',
    icon: 'fa fa-home',
    url: '#',
    submenu: [
      {
      name: 'Category',
      icon: 'fa fa-home',
      url: '/entity/category'
    },
      {
      name: 'Brand',
      icon: 'fa fa-home',
      url: '/entity/brand'
    },
  ]
  }]
},

];

  constructor() { }

  getShortCut() {

  }
}
