import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/Http';
import 'rxjs/add/operator/map';
import { UserService } from '../user.service';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  constructor(
    private http: HttpClient,
    private _UserService: UserService
  ) { }


  getAll(type: string, row: string, from: number) {
    let url = environment.URL_SERVICES + '/comment/' + type + '/' + row + '?from=' + from;
    return this.http.get(url, { params: { token: this._UserService.getToken(), access: 'read:any', table: 'comment' } })
      .map((resp: any) => resp);
  }
}
