import { Injectable } from '@angular/core';
import { UserService } from '../service.index';
import { environment } from '../../../environments/environment';

@Injectable()
export class SubirArchivoService {

  constructor(
    private _UserService: UserService
  ) { }


  subirArchivo(archivo: File, tipo: string, id: string) {

    return new Promise((resolve, reject) => {

      let formData = new FormData();
      let xhr = new XMLHttpRequest();
      formData.append('imagen', archivo, archivo.name);

      xhr.onreadystatechange = function () {

        if (xhr.readyState === 4) {

          if (xhr.status === 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }

        }
      };

      let url = environment.URL_SERVICES + '/upload/img/' + tipo + '/' + id + '?token=' + this._UserService.getToken() + '&access=create:any&table=img';

      xhr.open('PUT', url, true);
      xhr.send(formData);

    });




  }
  uploadFileExel(archivo: File, tipo: string, code: string = null) {

    return new Promise((resolve, reject) => {

      let formData = new FormData();
      let xhr = new XMLHttpRequest();

      formData.append('file', archivo, archivo.name);

      xhr.onreadystatechange = function () {

        if (xhr.readyState === 4) {

          if (xhr.status === 200) {
            console.log('Imagen subida');
            resolve(JSON.parse(xhr.response));
          } else {
            console.log('Fallo la subida');
            reject(xhr.response);
          }

        }
      };

      let url = environment.URL_SERVICES + '/upload/import/' + tipo + '/' + code + '?token=' + this._UserService.getToken() + '&access=create:any&table=' + tipo;

      xhr.open('PUT', url, true);
      xhr.send(formData);

    });
  }

  /**
   * Upload Multiplex Imagen for  different products
   * @param files  Array Imagen
   * @returns promise
   */
  upalodImagenProduct(files: File[]) {

    return new Promise((resolve, reject) => {

      let formData = new FormData();
      let xhr = new XMLHttpRequest();
      for (var i = 0; i < files.length; i++) {
        formData.append('file', files[i], files[i].name);
      }
      xhr.onreadystatechange = function () {

        if (xhr.readyState === 4) {

          if (xhr.status === 200) {
            console.log('Imagen subida');
            resolve(JSON.parse(xhr.response));
          } else {
            console.log('Fallo la subida');
            reject(xhr.response);
          }

        }
      };

      let url = environment.URL_SERVICES + '/upload/product/imagen' + '?token=' + this._UserService.getToken() + '&access=create:any&table=product';

      xhr.open('PUT', url, true);
      xhr.send(formData);

    });

  }
}
