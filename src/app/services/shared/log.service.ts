import { environment } from './../../../environments/environment';
import { UserService } from './../service.index';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/Http';
import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class LogService {
  path = `${environment.URL_SERVICES}/log`;

  constructor(
    private http: HttpClient,
    private _UserService: UserService
  ) { }

  getByEntityID(entity: string, id: string, from: number = 0, value: string = '', limit: number = 5, status: number = 1) {
    return this.http.get(`${this.path}/${entity}/${id}?from=${from}&limit=${limit}&value=${value}&status=${status}`,
      { params: { token: this._UserService.getToken(), access: 'read:any', table: 'log' } })
      .map((resp: any) => resp);
  }
}
