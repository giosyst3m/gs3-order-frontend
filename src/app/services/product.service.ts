import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/Http';
import 'rxjs/add/operator/map';
import { UserService } from './service.index';
import { environment } from '../../environments/environment';

@Injectable()
export class ProductService {

  constructor(
    private http: HttpClient,
    private _UserService: UserService
  ) { }

  getAll(from: number = 0, keywords: string = '', brand: string = '', line: string = '', category: string = '') {
    let url = environment.URL_SERVICES + '/catalog?from=' + from + '&value=' + keywords + '&brand=' + brand + '&line=' + line + '&category=' + category;
    return this.http.get(url, { params: { token: this._UserService.getToken(), access: 'read:any', table: 'catalog' } });
  }

  getByBarcode(barcode: string) {
    let url = environment.URL_SERVICES + '/product/barcode/' + barcode;
    return this.http.get(url, { params: { token: this._UserService.getToken(), access: 'read:any', table: 'catalog' } });
  }
}
