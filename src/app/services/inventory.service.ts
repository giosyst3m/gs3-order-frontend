import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/Http';
import 'rxjs/add/operator/map';
import { UserService } from './service.index';
import { environment } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class InventoryService {

  constructor(
    private http: HttpClient,
    private _UserService: UserService
  ) { }

  save(model: any) {
    let url = environment.URL_SERVICES + '/inventory/';

    return this.http.post(url, model, { params: { token: this._UserService.getToken(), access: 'create:any', table: 'inventory' } })
      .map((resp: any) => {
        return resp;
      });

  }

}
