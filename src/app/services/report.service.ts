import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/Http';
import 'rxjs/add/operator/map';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import { UserService } from './service.index';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

@Injectable({
  providedIn: 'root'
})
export class ReportService {
  path = `${environment.URL_SERVICES}`;

  constructor(
    private http: HttpClient,
    private _UserService: UserService
  ) { }

  getProductToExcel(lines: string[]) {
    return this.http.post(`${this.path}/export/product/excel`, { lines: lines }, { params: { token: this._UserService.getToken(), access: 'read:any', table: 'exportProductExcel' } })
      .map((resp: any) => resp);
  }

  getProductToPDF(email: string = 'no-email', company: string, lines: string[], img: boolean = true) {
    return this.http.post(`${this.path}/export/product/pdf/${email}/${img}`,
      { lines: lines },
      { params: { token: this._UserService.getToken(), access: 'read:any', table: 'exportProductPDF', company: company } })
      .map((resp: any) => resp);
  }

  public exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], { type: EXCEL_TYPE });
    FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
  }
}
