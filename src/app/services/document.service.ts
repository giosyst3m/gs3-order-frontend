
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/Http';
import 'rxjs/add/operator/map';
import { UserService } from './service.index';
import { environment } from '../../environments/environment';

@Injectable()
export class DocumentService {

  constructor(
    private http: HttpClient,
    private _UserService: UserService
  ) { }

  getByClient(client: string, company: string, type: string = 'Order', status: number = 1) {
    let url = environment.URL_SERVICES + '/document/client/' + client + '/' + type + '/' + status + '/' + company;
    return this.http.get(url, { params: { token: this._UserService.getToken(), access: 'read:any', table: 'client' } });
  }

  createHeader(model: any, type: string, status: number) {
    let url = environment.URL_SERVICES + '/document/header/' + type + '/' + status;
    return this.http.post(url, model, { params: { token: this._UserService.getToken(), access: 'create:any', table: 'document' } })
      .map((resp: any) => {
        return resp;
      });
  }

  updateCart(number) {
    let orderCart = document.getElementById('orderCart');
    orderCart.classList.remove('hidden');
    let orderNumber = document.getElementById('orderNumber');
    orderNumber.textContent = number;
  }

  getDocumentActive() {
    let orderNumber = document.getElementById('orderNumber');
    return Number(orderNumber.textContent);
  }

  addCart(model) {
    let url = environment.URL_SERVICES + '/document/detail/' + model.document;
    return this.http.post(url, model, { params: { token: this._UserService.getToken(), access: 'create:any', table: 'document' } })
      .map((resp: any) => {
        return resp;
      });
  }

  getById(id: string) {
    let url = environment.URL_SERVICES + '/document/' + id;
    return this.http.get(url, { params: { token: this._UserService.getToken(), access: 'read:any', table: 'document' } })
      .map((resp: any) => {
        return resp;
      });
  }

  getTotalsById(id: string) {
    let url = environment.URL_SERVICES + '/document/totals/' + id;
    return this.http.get(url, { params: { token: this._UserService.getToken(), access: 'read:any', table: 'document' } })
      .map((resp: any) => {
        return resp;
      });
  }

  getDetail(id: string, from: number = 0, limit: number = 15) {
    let url = environment.URL_SERVICES + '/document/detail/' + id + '?from=' + from + '&limit=' + limit;
    return this.http.get(url, { params: { token: this._UserService.getToken(), access: 'read:any', table: 'document' } });
  }

  deleteDeatilInZero(detail: string[]) {
    let url = environment.URL_SERVICES + '/document/detail/inZero/';

    return this.http.post(url, detail, { params: { token: this._UserService.getToken(), access: 'delete:any', table: 'detail' } })
      .map((resp: any) => resp.data);
  }

  deleteDetailNoInventory(document: string) {
    let url = environment.URL_SERVICES + '/document/detail/' + document;

    return this.http.delete(url, { params: { token: this._UserService.getToken(), access: 'delete:any', table: 'detail' } })
      .map((resp: any) => resp.data);
  }

  getByNumber(number: number, doc: string = 'Order') {
    let url = environment.URL_SERVICES + '/document/number/' + number + '/' + doc;

    return this.http.get(url, { params: { token: this._UserService.getToken(), access: 'read:any', table: 'document' } })
      .map((resp: any) => resp);
  }

  getPDFByID(number: string, data: any, doc: string = 'Order') {
    let url = environment.URL_SERVICES + '/document/pdf/' + number + '/' + doc;

    return this.http.patch(url, data, { params: { token: this._UserService.getToken(), access: 'read:any', table: 'document' } })
      .map((resp: any) => resp);
  }

  /**
   * Create a record
   * Update a record  by Id
   * @param table string name of entity
   * @param model array data to save.
   * @return object {Status, Result, Data, [error]}
   */
  save(model: any) {
    let url = environment.URL_SERVICES + '/document';
    if (model._id) {// Update

      return this.http.put(`${url}/${model._id}`, model, { params: { token: this._UserService.getToken(), access: 'update:any', table: 'document' } })
        .map((resp: any) => {
          return resp;

        });

    } else { // Create
      return this.http.post(`${url}/`, model, { params: { token: this._UserService.getToken(), access: 'create:any', table: 'document' } })
        .map((resp: any) => {
          return resp;
        });
    }
  }

  getByStatus(status: string[]) {
    let url = environment.URL_SERVICES + '/document/status';
    return this.http.post(url, { status: status }, { params: { token: this._UserService.getToken(), access: 'create:any', table: 'document' } })
      .map((resp: any) => {
        return resp;
      });
  }

}
