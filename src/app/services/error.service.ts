import { PNotifyService } from './shared/p-notify.service';
import { Injectable } from '@angular/core';

@Injectable()
export class ErrorService {
  pnotify = this._pnotify.getPNotify();

  constructor(
    private _pnotify: PNotifyService,
  ) { }

  showErrors(err: any) {
    if (err.error.err.errors) {
      for (const key in err) {
        if (err.error.err.errors.hasOwnProperty(key)) {
          const element = err.error.err.errors[key];
          this.pnotify.error({
            title: `El Campo: ${element.path} Error Tipo ${element.kind} `,
            text: `${element.message}`
          });
        }
      }
    }

    if (err.error.err.message) {
      this.pnotify.error({
        title: err.error.err.title || `Error`,
        text: `${err.error.err.message}`
      });
    }
  }

  showErrorDuplicate(err: any) {
    this.pnotify.error({
      title: 'Dato Duplicado',
      text: err.error.err.errmsg
    });
  }

  showErroMessage(error) {
    this.pnotify.error({
      title: `El Error presentado `,
      text: `${error.error.error.message}`
    });
  }
}

