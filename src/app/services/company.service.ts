import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/Http';
import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {
  path = `${environment.URL_SERVICES}/company`;

  constructor(
    private http: HttpClient
  ) { }

  getCompanySelect() {
    let company = document.getElementById('companySelect');
    for (let index = 0; index < company.childElementCount; index++) {
      const element = company.children[index];
      if (element['selected']) {
        return element['value'];
      }
    }
  }

}

