import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/Http';
import 'rxjs/add/operator/map';
import { User } from '../models/user.model';
import { Router } from '@angular/router';
import { NgxPermissionsService, NgxRolesService } from 'ngx-permissions';


@Injectable({
  providedIn: 'root'
})
export class UserService {
  path = `${environment.URL_SERVICES}/user`;
  user: User;
  token: string;

  constructor(
    private http: HttpClient,
    private router: Router,
    private permissionsService: NgxPermissionsService,
    private roleService: NgxRolesService

  ) {
    this.getStorage();
    this.token = this.getToken();
  }

  /**
    * Create/update a user
    * @param model array data to save.
    * @return object {Status, Result, Data, [error]}
    */
  save(model: any) {
    model['token'] = this.token;
    if (model._id) {// Update

      return this.http.put(`${this.path}/${model._id}`, model)
        .map((resp: any) => {
          return resp;

        });

    } else { // Create
      return this.http.post(`${this.path}`, model)
        .map((resp: any) => {
          return resp;
        });
    }
  }

  /**
   * User Login
   * @param model Array User data
   * @param remember boolean if user wants to remember email
   * @return boolea TRUE = OK FALSE = ERROR
   */
  login(model: User, remember: boolean = false) {
    this.setRememeber(remember, model.email);
    return this.http.post(`${environment.URL_SERVICES}/login`, model)
      .map((resp: any) => {
        this.setStorage(resp.data);
        this.loadPermissions();
        this.user = resp.data;
        this.token = resp.data.token;
        return true;
      });
  }

  loadPermissions() {
    this.user = this.getUser();
    this.permissionsService.loadPermissions([this.user.role]);
    this.permissionsService.addPermission(['listType', 'editType']);
    this.roleService.addRole(this.user.role, ['listType', 'editType']);
  }


  /**
   * User Logout
   * Remove whole localstore relate to user.
   * @return send to login page
   */
  logout() {
    this.user = null;
    this.token = '';
    this.removeStorage();
    this.router.navigate(['/login']);
    this.roleService.flushRoles();
  }

  /**
   * Valid if user is Login or not
   * @return boolean
   */
  isLogin() {
    console.log(this.token);
    if (this.token) {
      return (this.token.length > 10) ? true : false;
    } else {
      return false;
    }
  }

  /**
   * Set Localsetore to remember email to user when is going to login
   * @param remember boolean
   * @param email string
   */
  setRememeber(remember, email) {
    if (remember) {
      localStorage.setItem('gs3_email', email);
    } else {
      localStorage.removeItem('gs3_email');
    }
  }

  /**
   * Remove localStore relate to user
   */
  removeStorage() {
    localStorage.removeItem('gs3_token');
    localStorage.removeItem('gs3_id');
    localStorage.removeItem('gs3_user');
  }

  /**
   * Set localStore user is loging
   * @param data array User data
   */
  setStorage(data) {
    localStorage.setItem('gs3_id', data._id);
    localStorage.setItem('gs3_token', data.token);
    localStorage.setItem('gs3_user', JSON.stringify(data));
  }

  /**
   * Get localStore user is logined
   */
  getStorage() {
    if (localStorage.getItem('gs3_token')) {
      this.token = localStorage.getItem('gs3_token');
      this.user = JSON.parse(localStorage.getItem('gs3_user'));
    } else {
      this.token = '';
      this.user = null;
    }
  }

  getUser() {
    return JSON.parse(localStorage.getItem('gs3_user'));
  }

  /**
   * Return Token from localStore
   */
  getToken() {
    return localStorage.getItem('gs3_token');
  }

  getRols() {
    return [
      {
        id: '_Gerente_',
        name: 'Gerencia'
      },
      {
        id: '_BODEGA_',
        name: 'Bodega'
      },
      {
        id: '_VENTAS_',
        name: 'Ventas'
      },
      {
        id: '_CLIENTE_',
        name: 'Cliente'
      },
    ];
  }

  /**
   * Change Password a User by ID
   * @param id string User ID
   * @param password string New password ID
   */
  reseatPassword(id: string, password: string) {
    return this.http.post(`${this.path}/reseat`, { id: id, password: password, token: this.token })
      .map((resp: any) => {
        return resp;
      });
  }

  /**
   * Change Password a User by email
   * @param email string User email
   * @param password string New password
   */
  recoveryPassword(model: User) {
    return this.http.patch(`${this.path}/reseat`, model)
      .map((resp: any) => {
        return resp;
      });
  }
}
