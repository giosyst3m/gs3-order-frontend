import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/Http';
import { RECORDS_LIMIT } from '../config/config';
import 'rxjs/add/operator/map';
import { UserService } from './service.index';
import { environment } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(
    private http: HttpClient,
    private _UserService: UserService
  ) { }

  getTotalGroupBy(group: string = 'status', status: number = 0, limit: number = RECORDS_LIMIT, skip: number = 0) {
    let url = environment.URL_SERVICES + '/dashboard/group/' + group + '/' + status + '?limit=' + limit + '&skip=' + skip;
    return this.http.get(url, { params: { token: this._UserService.getToken(), access: 'read:any', table: 'dashboard' } })
      .map((resp: any) => resp);
  }

  getAllDocument(status: string, user: string, number: number = 0, skip: number = 0, limit: number = RECORDS_LIMIT, type: string = 'order') {
    let url = environment.URL_SERVICES + `/dashboard/document/'${type}?limit=${limit}&skip=${skip}`;
    if (number > 0) {
      url = url + `&number=${number}`;
    }
    return this.http.get(url, { params: { token: this._UserService.getToken(), access: 'read:any', table: 'dashboard', status, user } })
      .map((resp: any) => resp);
  }
}
