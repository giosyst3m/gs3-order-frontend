import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/Http';
import { RECORDS_LIMIT } from '../config/config';
import 'rxjs/add/operator/map';
import { UserService } from './service.index';


@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(
    private http: HttpClient,
    private _UserService: UserService
  ) { }

  /**
   * Get List records
   * @param from number from record wants to start
   * @param value string value to search
   * @param limit nubmer how many records wants to get Defoutl define by RECORDS_LIMIT
   * @param status number Get Records 1 = Active 2 = Inactive 3 = both
   * @return object {Status, Result, Data, TotalRecords, [error]}
   */
  getAll(from: number = 0, value: string = '', limit: number = RECORDS_LIMIT, status: number = 1) {
    return this.http.get(`${environment.URL_SERVICES}/client?from=${from}&limmit=${limit}&value=${value}&status=${status}`, { params: { token: this._UserService.getToken(), access: 'read:any', table: 'client' } })
      .map((resp: any) => resp);
  }

  /**
   * Get Record from Client by Id
   * @param id strig of entity
   * @return object {Status, Result, Data, [error]}
   */
  getById(id: string) {

    return this.http.get(`${environment.URL_SERVICES}/client/${id}`, { params: { token: this._UserService.getToken(), access: 'read:any', table: 'client' } })
      .map((resp: any) => resp);
  }
}
