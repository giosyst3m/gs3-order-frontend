import { UserService } from '../services/service.index';
import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AccessGuard implements CanActivate {
  constructor(
    private _UserService: UserService,
  ) {

  }
  canActivate() {
    if ( this._UserService.user.role === '_ADMIN_' ) {
      return true;
    } else {
      this._UserService.logout();
      return false;
    }
  }
}
