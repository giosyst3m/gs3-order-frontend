import { UserService } from './../services/service.index';
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate {

  constructor(
    private _UserService: UserService,
    private router: Router
  ) {

  }
  canActivate() {
    if ( this._UserService.isLogin() ) {
      return true;
    } else {
      this.router.navigate(['/login']);
      return false;
    }
  }
}
