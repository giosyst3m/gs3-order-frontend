import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  LoginGuard,
  AccessGuard

} from './index.guards';


@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers: [
    LoginGuard,
    AccessGuard
  ]
})
export class GuardsModule { }
