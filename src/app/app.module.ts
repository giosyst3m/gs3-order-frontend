// Modules
import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { PagesModule } from './pages/pages.module';
import { UserModule } from './user/user.module';
import { DataTablesModule } from 'angular-datatables';
import { GuardsModule } from './guards/guards.module';

// Services
import { ServiceModule } from './services/service.module';

// Component
import { AppComponent } from './app.component';




// Rutas
import { APP_ROUTES } from './app.routes';
import { HttpClientModule } from '@angular/common/Http';






@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    APP_ROUTES,
    PagesModule,
    UserModule,
    ServiceModule,
    HttpClientModule,
    DataTablesModule,
    GuardsModule
  ],
  providers: [Title],
  bootstrap: [AppComponent]
})
export class AppModule { }
