import { PipesModule } from '../pipes/pipes.module';
import { NgModule } from '@angular/core';

import { LoginComponent } from './login/login.component';
import { UserComponent } from './user.component';
import { USER_ROUTES } from './user.routes';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RecoveryComponent } from './recovery/recovery.component';




@NgModule({
    declarations: [
        LoginComponent,
        UserComponent,
        RecoveryComponent,
    ],
    exports: [
        LoginComponent
    ],
    imports: [
        USER_ROUTES,
        PipesModule,
        FormsModule,
        ReactiveFormsModule,
    ]
})
export class UserModule { }
