import { Routes, RouterModule  } from '@angular/router';
import { UserComponent } from './user.component';
import { LoginComponent } from './login/login.component';
import { RecoveryComponent } from './recovery/recovery.component';


const UserRoutes: Routes = [
    {
        path: '',
        component: UserComponent,
        children: [
            {path: 'login', component: LoginComponent},
            {path: 'recovery', component: RecoveryComponent},
            {path: '', redirectTo: '/dashboard', pathMatch: 'full' },
        ]
    },
];

export const USER_ROUTES = RouterModule.forChild( UserRoutes );
