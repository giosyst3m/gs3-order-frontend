import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { takeUntil } from 'rxjs/operators';
import { SYSTEM_EMAIL } from './../../config/config';
import swal from 'sweetalert2';
import { NgForm } from '@angular/forms';
import { User } from '../../models/index.models';
import { UserService } from '../../services/service.index';
import { Router } from '@angular/router';
import { APP_NAME } from '../../config/config';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-recovery',
  templateUrl: './recovery.component.html',
  styleUrls: ['./recovery.component.css']
})
export class RecoveryComponent implements OnInit, OnDestroy {
  destroySubject$: Subject<void> = new Subject();
  SYSTEM_EMAIL = SYSTEM_EMAIL;
  email: string;
  constructor(
    private _UserService: UserService,
    public router: Router,
    private titleService: Title
  ) {
    this.titleService.setTitle(APP_NAME + 'Recuperación de contraseña');
  }

  ngOnInit() {
  }

  recovery(form: NgForm) {
    if (form.valid) {
      let user = new User(null, null, form.value.email, null, null, null);
      this._UserService.recoveryPassword(user)
        .pipe(takeUntil(this.destroySubject$))
        .subscribe((resp: any) => {
          swal({
            title: 'Confirmación',
            html: `Se ha enviado a su correo <b>${resp.data.email}</b> con su nueva contraseña`,
            type: 'success'
          });
          this.router.navigate(['/login']);
        }, (err) => {
          swal({
            title: 'Error al recuperar la contraseña',
            html: err.error.error.message,
            type: 'error'
          });
        });
    } else {
      swal({
        title: 'Dato requerido',
        html: 'Debe colocar el email para poder recuperar la contraseña',
        type: 'warning'
      });
    }
  }
  ngOnDestroy(): void {
    this.destroySubject$.next();
    this.destroySubject$.complete();
  }
}
