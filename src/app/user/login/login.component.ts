import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { takeUntil } from 'rxjs/operators';
import { SYSTEM_EMAIL } from './../../config/config';
import { NgForm } from '@angular/forms';
import { UserService, PNotifyService } from '../../services/service.index';
import { User } from '../../models/index.models';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { APP_NAME } from '../../config/config';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {
  destroySubject$: Subject<void> = new Subject();
  remember = false;
  pnotify = this._pnotify.getPNotify();
  email: string;
  SYSTEM_EMAIL = SYSTEM_EMAIL;
  constructor(
    private _UserService: UserService,
    public router: Router,
    private _pnotify: PNotifyService,
    private titleService: Title
  ) {
    this.titleService.setTitle(APP_NAME + 'LogIn');
  }

  ngOnInit() {
    this.email = localStorage.getItem('gs3_email') || '';
    if (this.email.length > 0) {
      this.remember = true;
    }
  }

  login(form: NgForm) {
    if (form.invalid) {
      return;
    }
    let user = new User(null, null, form.value.email, null, null, null, form.value.password);
    this._UserService.login(user, form.value.remember)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((resp) => {
        this.pnotify.success({
          title: 'Inicio de Sesión',
          text: 'Datos Validados correctamente'
        });
        this.router.navigate(['/dashboard']);
      }, (err) => {
        swal('Error al hacer login', 'Favor revisar sus credenciales e intentar nuevamente', 'error');
      });
  }
  ngOnDestroy(): void {
    this.destroySubject$.next();
    this.destroySubject$.complete();
  }
}
