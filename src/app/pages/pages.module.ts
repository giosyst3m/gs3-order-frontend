import { GuardsModule } from './../guards/guards.module';
import { PasswordComponent } from './team/password.component';
// Routes
import { PAGES_ROUTES } from './pages.routes';
// Modulies
import { NgModule } from '@angular/core';
import { ErrorModule } from '../error/error.module';
import { SharedModule } from '../shared/shared.module';
import { CommonModule } from '@angular/common';
import { DataTablesModule } from 'angular-datatables';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPermissionsModule } from 'ngx-permissions';

// Pipes
import { PipesModule } from '../pipes/pipes.module';

// Charts
import { NgxEchartsModule } from 'ngx-echarts';

// Cloudinary
import { Cloudinary } from 'cloudinary-core';
import { CloudinaryModule, CloudinaryConfiguration } from '@cloudinary/angular-5.x';


// Component
import { DashboardComponent } from './dashboard/dashboard.component';
import { PagesComponent } from './pages.component';
import { ProductComponent } from './product/product.component';
import { TypeComponent } from './type/type.component';
import { ClientComponent } from './client/client.component';
import { CatalogComponent } from './catalog/catalog.component';
import { DocumentComponent } from './document/document.component';
import { DeliveyComponent } from './delivey/delivey.component';
import { DocumentRelateComponent } from './document-relate/document-relate.component';
import { ImportComponent } from './import/import.component';
import { LineComponent } from './line/line.component';
import { CategoryComponent } from './category/category.component';
import { BrandComponent } from './brand/brand.component';
import { CityComponent } from './city/city.component';
import { StateComponent } from './state/state.component';
import { ZoneComponent } from './zone/zone.component';
import { ImportClientComponent } from './import/import-client.component';
import { ImportProductComponent } from './import/import-product.component';
import { CounterComponent } from './counter/counter.component';
import { CompanyComponent } from './company/company.component';
import { TaxComponent } from './tax/tax.component';
import { StatusComponent } from './status/status.component';
import { ImportBillComponent } from './import/import-bill.component';
import { ImportImagenComponent } from './import/import-imagen.component';
import { ReportProductComponent } from './report/report-product.component';
import { CatalogSimpleComponent } from './catalog/catalog-simple.component';
import { TeamComponent } from './team/team.component';
import { RoleComponent } from './role/role.component';
import { AccessComponent } from './access/access.component';
import { DiscountComponent } from './discount/discount.component';
import { TycComponent } from './tyc/tyc.component';
import { ProfileComponent } from './profile/profile.component';

@NgModule({
    declarations: [
        PagesComponent,
        DashboardComponent,
        ProductComponent,
        TypeComponent,
        ClientComponent,
        CatalogComponent,
        DocumentComponent,
        DeliveyComponent,
        DocumentRelateComponent,
        ImportComponent,
        LineComponent,
        CategoryComponent,
        BrandComponent,
        CityComponent,
        StateComponent,
        ZoneComponent,
        ImportClientComponent,
        ImportProductComponent,
        CounterComponent,
        CompanyComponent,
        TaxComponent,
        StatusComponent,
        ImportImagenComponent,
        ImportBillComponent,
        ReportProductComponent,
        CatalogSimpleComponent,
        TeamComponent,
        PasswordComponent,
        RoleComponent,
        AccessComponent,
        DiscountComponent,
        TycComponent,
        ProfileComponent
    ],
    exports: [
        DashboardComponent,
        PagesComponent,
        CloudinaryModule,
        NgxPermissionsModule
    ],
    imports: [
        CommonModule,
        SharedModule,
        ErrorModule,
        PAGES_ROUTES,
        PipesModule,
        DataTablesModule,
        FormsModule,
        ReactiveFormsModule,
        NgxEchartsModule,
        GuardsModule,
        CloudinaryModule.forRoot({Cloudinary}, { cloud_name: 'giosyst3m' } as CloudinaryConfiguration),
        NgxPermissionsModule.forRoot()
    ]
})

export class PagesModule { }
