import { DashboardService, ErrorService, EntityService } from '../../services/service.index';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { TotalGroupBy } from '../../interface/interface-index';
import { Status, User } from '../../models/index.models';
import { Title } from '@angular/platform-browser';
import { APP_NAME } from 'src/app/config/config';
import { Subject } from 'rxjs/Subject';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, OnDestroy {
  TotalGroupBy: TotalGroupBy[] = [];
  document: any[] = [];
  lodaing = true;
  estados: Status[] = [];
  total: number;
  from = 0;
  users: User[] = [];
  chartsOptions: any;
  chartData1: any[];
  chartxAxisData: any[];
  destroySubject$: Subject<void> = new Subject();

  constructor(
    private _DashboarS: DashboardService,
    private _ErrorS: ErrorService,
    private _EntityService: EntityService,
    private titleService: Title
  ) {
    this.titleService.setTitle(APP_NAME + 'Dashboard');
    // this.getTotalGroupByStatus();
    this.getAllDocument();
    // this.charts();
  }

  charts(month: number = 0) {

    this._DashboarS.getTotalGroupBy('month', month)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((resp: any) => {
        this.chartData1 = resp.total;
        this.chartxAxisData = resp.monthName;

        this.chartsOptions = {
          color: [
            '#26B99A', '#34495E', '#BDC3C7', '#3498DB',
            '#9B59B6', '#8abb6f', '#759c6a', '#bfd3b7'
          ],
          legend: {
            data: ['Pedidos'],
            align: 'left'
          },
          tooltip: {},
          xAxis: {
            data: this.chartxAxisData,
            silent: false,
            splitLine: {
              show: false
            }
          },
          yAxis: {
          },
          series: [{
            label: {
              show: true
            },
            name: 'Validar',
            type: 'bar',
            data: this.chartData1,
            animationDelay: function (idx) {
              return idx * 10;
            }
          }],
          animationEasing: 'elasticOut',
          animationDelayUpdate: function (idx) {
            return idx * 5;
          }
        };
      });
  }

  getTotalGroupByStatus(group: string = 'status') {
    this._DashboarS.getTotalGroupBy(group)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((resp: any) => {
        console.log(resp);
        this.TotalGroupBy = resp.data;
      }, (err) => {
        this._ErrorS.showErrors(err);
      });
  }

  getAllDocument(status: string = '', user: string = '', number: number = 0, from: number = this.from) {
    this.lodaing = true;
    this.from = from;
    this._DashboarS.getAllDocument(status, user, number, this.from)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((resp: any) => {
        this.document = resp.data;
        this.total = resp.total;
        this.lodaing = false;
      }, (err) => {
        this._ErrorS.showErrors(err);
      });
  }

  ngOnInit() {

    this._EntityService.getAll('status')
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((resp: any) => {
        this.estados = resp.data;
      });
    this._EntityService.getAll('user')
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((resp: any) => {
        this.users = resp.data;
      });
  }

  chageFrom(value) {
    let from = this.from + value;
    if (from >= this.total) {
      return;
    }

    if (from < 0) {
      return;
    }

    this.from += value;
    this.getAllDocument();
  }
  ngOnDestroy(): void {
    this.destroySubject$.next();
    this.destroySubject$.complete();
  }
}
