import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { takeUntil } from 'rxjs/operators';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PNotifyService, DocumentRelateService, EntityService } from '../../services/service.index';
import { DocumentRelate } from '../../models/index.models';


@Component({
  selector: 'app-document-relate',
  templateUrl: './document-relate.component.html',
  styleUrls: ['./document-relate.component.css']
})
export class DocumentRelateComponent implements OnInit, OnDestroy {
  destroySubject$: Subject<void> = new Subject();
  documentRelate: DocumentRelate = new DocumentRelate(null, null, null, null);
  documentsRelate: DocumentRelate[];

  pnotify = this._pnotify.getPNotify();
  form: FormGroup;
  totalRecords = 0;
  from = 0;

  @Input() document: string;
  @Input() billShow: boolean;
  @Input() billForm: boolean;

  constructor(
    private _pnotify: PNotifyService,
    private _documentRelateS: DocumentRelateService,
    private _EntityS: EntityService
  ) {

    this.form = new FormGroup({
      number: new FormControl('', Validators.required),
      price: new FormControl(0, [Validators.required]),
      status: new FormControl(true, [Validators.required])
    });
    setTimeout(() => {
      this.getAll();
    }, 4000);

  }

  ngOnInit() {
  }

  getAll() {
    this._documentRelateS.getAllByDocument(this.document, this.from)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((resp: any) => {
        this.documentsRelate = resp.data;
        this.totalRecords = resp.total;
      });
  }
  save() {
    if (this.form.invalid) {
      return;
    }
    this._documentRelateS.save(this.document, this.form.value)
      .subscribe((resp) => {
        this.pnotify.success({
          title: 'Confirmación',
          text: 'Se Agergó la fatura '
        });
        this.form.reset({
          _id: null,
          name: null,
          price: 0,
          status: true,
          __v: null,
        });
        this.getAll();
      }, (err) => {
        console.log(err);
        this.pnotify.error({
          title: 'Error',
          text: err
        });
      });
  }

  delete(id: string) {
    this._EntityS.delete('documentrelate', id)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((resp) => {
        this.pnotify.success({
          title: 'Confirmación',
          text: 'Se Eliminó la Fatura '
        });
        this.getAll();
      });
  }
  chageFromDR(value) {
    const from = this.from + value;
    if (from >= this.totalRecords) {
      return;
    }

    if (from < 0) {
      return;
    }

    this.from += value;
    this.getAll();
  }
  ngOnDestroy(): void {
    this.destroySubject$.next();
    this.destroySubject$.complete();
  }

}
