// Guards
import { NgxPermissionsGuard } from 'ngx-permissions';
import { LoginGuard, AccessGuard } from '../guards/index.guards';

// Component
import { TaxComponent } from './tax/tax.component';
import { CategoryComponent } from './category/category.component';
import { ImportComponent } from './import/import.component';
import { DocumentIMPComponent } from '../shared/document-imp/document-imp.component';
import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProductComponent } from './product/product.component';
import { TypeComponent } from './type/type.component';
import { ClientComponent } from './client/client.component';
import { CatalogComponent } from './catalog/catalog.component';
import { DocumentComponent } from './document/document.component';
import { LineComponent } from './line/line.component';
import { BrandComponent } from './brand/brand.component';
import { CityComponent } from './city/city.component';
import { StateComponent } from './state/state.component';
import { ZoneComponent } from './zone/zone.component';
import { CounterComponent } from './counter/counter.component';
import { CompanyComponent } from './company/company.component';
import { StatusComponent } from './status/status.component';
import { ImportImagenComponent } from './import/import-imagen.component';
import { ReportProductComponent } from './report/report-product.component';
import { CatalogSimpleComponent } from './catalog/catalog-simple.component';
import { TeamComponent } from './team/team.component';
import { RoleComponent } from './role/role.component';
import { AccessComponent } from './access/access.component';
import { DiscountComponent } from './discount/discount.component';
import { TycComponent } from './tyc/tyc.component';
import { ProfileComponent } from './profile/profile.component';



const PagesRoutes: Routes = [
    {
        path: '',
        component: PagesComponent,
        canActivate: [ LoginGuard ],
        children: [
            {
                path: 'dashboard',
                component: DashboardComponent,
                canActivate: [LoginGuard],
                data: {
                    permissions: {
                      only: ['_ADMIN_', '_GERENTE_', '_VENTAS_', '_BODEGA_'],
                      redirectTo: '/login'
                    }
                  }
            },
            {
                path: 'product',
                component: ProductComponent,
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                      only: ['_ADMIN_', '_GERENTE_'],
                      redirectTo: '/dashboard'
                    }
                  }
            },
            {
                path: 'product/:id',
                component: ProductComponent,
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                      only: ['_ADMIN_', '_GERENTE_'],
                      redirectTo: '/dashboard'
                    }
                  }
            },
            {
                path: 'client',
                component: ClientComponent,
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                      only: ['_ADMIN_', '_GERENTE_'],
                      redirectTo: '/dashboard'
                    }
                  }
            },
            {
                path: 'client/:id',
                component: ClientComponent,
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                      only: ['_ADMIN_', '_GERENTE_'],
                      redirectTo: '/dashboard'
                    }
                  }
            },
            {
                path: 'city',
                component: CityComponent,
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                      only: ['_ADMIN_', '_GERENTE_'],
                      redirectTo: '/dashboard'
                    }
                  }
            },
            {
                path: 'city/:id',
                component: CityComponent,
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                      only: ['_ADMIN_', '_GERENTE_'],
                      redirectTo: '/dashboard'
                    }
                  }
            },
            {
                path: 'state',
                component: StateComponent,
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                      only: ['_ADMIN_', '_GERENTE_'],
                      redirectTo: '/dashboard'
                    }
                  }
            },
            {
                path: 'state/:id',
                component: StateComponent,
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                      only: ['_ADMIN_', '_GERENTE_'],
                      redirectTo: '/dashboard'
                    }
                  }
            },
            {
                path: 'zone',
                component: ZoneComponent,
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                      only: ['_ADMIN_', '_GERENTE_'],
                      redirectTo: '/dashboard'
                    }
                  }
            },
            {
                path: 'zone/:id',
                component: ZoneComponent,
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                      only: ['_ADMIN_', '_GERENTE_'],
                      redirectTo: '/dashboard'
                    }
                  }
            },
            {
                path: 'type',
                component: TypeComponent,
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                      only: ['_ADMIN_', '_GERENTE_'],
                      redirectTo: '/dashboard'
                    }
                  }
            },
            {
                path: 'type/:id',
                component: TypeComponent,
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                      only: ['_ADMIN_', '_GERENTE_'],
                      redirectTo: '/dashboard'
                    }
                  }
            },
            {
                path: 'discount',
                component: DiscountComponent,
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                      only: ['_ADMIN_', '_GERENTE_'],
                      redirectTo: '/dashboard'
                    }
                  }
            },
            {
                path: 'discount/:id',
                component: DiscountComponent,
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                      only: ['_ADMIN_', '_GERENTE_'],
                      redirectTo: '/dashboard'
                    }
                  }
            },
            {
                path: 'catalog',
                component: CatalogComponent,
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                      only: ['_ADMIN_', '_GERENTE_', '_VENTAS_', '_BODEGA_', '_CLIENTE_'],
                      redirectTo: '/dashboard'
                    }
                  }
            },
            {
                path: 'catalog/simple',
                component: CatalogSimpleComponent,
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                      only: ['_ADMIN_', '_GERENTE_', '_VENTAS_', '_BODEGA', '_CLIENTE_'],
                      redirectTo: '/dashboard'
                    }
                  }
            },
            {
                path: 'import/imagen',
                component: ImportImagenComponent,
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                      only: ['_ADMIN_', '_GERENTE_'],
                      redirectTo: '/dashboard'
                    }
                  }
            },
            {
                path: 'import/:type',
                component: ImportComponent,
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                      only: ['_ADMIN_', '_GERENTE_'],
                      redirectTo: '/dashboard'
                    }
                  }
            },
            {
                path: 'doc/imp/:id',
                component: DocumentIMPComponent,
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                      only: ['_ADMIN_', '_GERENTE_', '_VENTAS_', '_BODEGA_'],
                      redirectTo: '/dashboard'
                    }
                  }
            },
            {
                path: 'doc/:type/:id',
                component: DocumentComponent,
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                      only: ['_ADMIN_', '_GERENTE_', '_VENTAS_', '_BODEGA_', '_CLIENTE_'],
                      redirectTo: '/dashboard'
                    }
                  }
            },
            {
                path: 'line',
                component: LineComponent,
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                      only: ['_ADMIN_', '_GERENTE_'],
                      redirectTo: '/dashboard'
                    }
                  }
            },
            {
                path: 'line/:id',
                component: LineComponent,
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                      only: ['_ADMIN_', '_GERENTE_'],
                      redirectTo: '/dashboard'
                    }
                  }
            },
            {
                path: 'category',
                component: CategoryComponent,
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                      only: ['_ADMIN_', '_GERENTE_'],
                      redirectTo: '/dashboard'
                    }
                  }
            },
            {
                path: 'category/:id',
                component: CategoryComponent,
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                      only: ['_ADMIN_', '_GERENTE_'],
                      redirectTo: '/dashboard'
                    }
                  }
            },
            {
                path: 'brand',
                component: BrandComponent,
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                      only: ['_ADMIN_', '_GERENTE_'],
                      redirectTo: '/dashboard'
                    }
                  }
            },
            {
                path: 'brand/:id',
                component: BrandComponent,
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                      only: ['_ADMIN_', '_GERENTE_'],
                      redirectTo: '/dashboard'
                    }
                  }
            },
            {
                path: 'counter',
                component: CounterComponent,
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                      only: ['_ADMIN_'],
                      redirectTo: '/dashboard'
                    }
                  }
            },
            {
                path: 'counter/:id',
                component: CounterComponent,
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                      only: ['_ADMIN_'],
                      redirectTo: '/dashboard'
                    }
                  }
            },
            {
                path: 'company',
                component: CompanyComponent,
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                      only: ['_ADMIN_', '_GERENTE_'],
                      redirectTo: '/dashboard'
                    }
                  }
            },
            {
                path: 'company/:id',
                component: CompanyComponent,
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                      only: ['_ADMIN_', '_GERENTE_'],
                      redirectTo: '/dashboard'
                    }
                  }
            },
            {
                path: 'tax', component: TaxComponent,
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                      only: ['_ADMIN_', '_GERENTE_'],
                      redirectTo: '/dashboard'
                    }
                  }
            },
            {
                path: 'tax/:id',
                component: TaxComponent,
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                      only: ['_ADMIN_', '_GERENTE_'],
                      redirectTo: '/dashboard'
                    }
                  }
            },
            {
                path: 'status', component: StatusComponent,
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                      only: ['_ADMIN_', '_GERENTE_'],
                      redirectTo: '/dashboard'
                    }
                  }
            },
            {
                path: 'status/:id',
                component: StatusComponent,
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                      only: ['_ADMIN_', '_GERENTE_'],
                      redirectTo: '/dashboard'
                    }
                  }
            },
            {
                path: 'export/product', component: ReportProductComponent,
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                      only: ['_ADMIN_', '_GERENTE_','_CLIENTE_', '_VENTAS_'],
                      redirectTo: '/dashboard'
                    }
                  }
            },
            {
                path: 'team',
                component: TeamComponent,
                canActivate: [NgxPermissionsGuard],
                data: {
                    permissions: {
                      only: ['_ADMIN_', '_GERENTE_'],
                      redirectTo: '/dashboard'
                    }
                  }
            },
            {
                path: 'team/:id',
                canActivate: [NgxPermissionsGuard],
                component: TeamComponent,
                data: {
                    permissions: {
                      only: ['_ADMIN_', '_GERENTE_'],
                      redirectTo: '/dashboard'
                    }
                  }
            },
            {
                path: 'role',
                canActivate: [NgxPermissionsGuard],
                component: RoleComponent,
                data: {
                    permissions: {
                      only: ['_ADMIN_'],
                      redirectTo: '/dashboard'
                    }
                  }
            },
            {
                path: 'role/:id',
                canActivate: [NgxPermissionsGuard],
                component: RoleComponent,
                data: {
                    permissions: {
                      only: ['_ADMIN_'],
                      redirectTo: '/dashboard'
                    }
                  }
            },
            {
                path: 'access',
                canActivate: [NgxPermissionsGuard],
                component: AccessComponent,
                data: {
                    permissions: {
                      only: ['_ADMIN_'],
                      redirectTo: '/dashboard'
                    }
                  }
            },
            {
                path: 'access/:id',
                canActivate: [NgxPermissionsGuard],
                component: AccessComponent,
                data: {
                    permissions: {
                      only: ['_ADMIN_'],
                      redirectTo: '/dashboard'
                    }
                  }
            },
            {
                path: 'terminos-condiciones',
                component: TycComponent,
                data: {
                    permissions: {
                    }
                  }
            },
            {
              path: 'profile',
              canActivate: [NgxPermissionsGuard],
              component: ProfileComponent,
              data: {
                  permissions: {
                    only: ['_ADMIN_', '_GERENTE_', '_CLIENTE_', '_VENTAS_', '_BODEGA_'],
                    redirectTo: '/dashboard'
                  }
                }
          },
            {path: '', redirectTo: '/dashboard', pathMatch: 'full' },
        ]
    },
];

export const PAGES_ROUTES = RouterModule.forChild( PagesRoutes );
