import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { takeUntil } from 'rxjs/operators';
import { APP_NAME } from 'src/app/config/config';
import { Title } from '@angular/platform-browser';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PNotifyService, UserService, EntityService, ErrorService } from './../../services/service.index';
import { UserLogin } from 'src/app/interface/interface-index';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit, OnDestroy {
  destroySubject$: Subject<void> = new Subject();
  pnotify = this._pnotify.getPNotify();
  UserLogin: UserLogin = null;
  form: FormGroup;


  constructor(
    private _pnotify: PNotifyService,
    private _UserService: UserService,
    private _EntityService: EntityService,
    private titleService: Title,
    private _errorS: ErrorService,
  ) {
    this.UserLogin = this._UserService.getUser();
    this.form = new FormGroup({
      name: new FormControl(null, [Validators.required]),
      lastname: new FormControl(null, [Validators.required]),
      email: new FormControl(null, [Validators.required, Validators.email]),
      role: new FormControl(null, [Validators.required]),
      password: new FormControl(null),
      avatar: new FormControl('avatar'),
      client: new FormControl(null),
      zone: new FormControl(null, [Validators.required]),
      company: new FormControl(null, [Validators.required]),
      status: new FormControl(true),
      _id: new FormControl(null),
      __v: new FormControl(),
    });
    this.titleService.setTitle(APP_NAME + 'Perfil');
    this._EntityService.getById('user', this.UserLogin._id)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((resp: any) => {
        this.form.setValue(resp);
      }, (err) => {
        this.pnotify.error({
          title: 'Error',
          text: 'No se pudo recuperar la información, intentar mas tarde ' + err.error.err.message
        });
      });
  }

  save() {
    this._UserService.save(this.form.value)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe(resp => {
        this.pnotify.success({
          title: 'Conformación',
          text: 'Datos Actualizados exitosamente'
        });

      }, (err) => {
        if (err.error.err.code === 11000) {
          this._errorS.showErrorDuplicate(err);
        } else {
          this._errorS.showErrors(err);
        }
      });
  }
  ngOnInit() {
  }
  ngOnDestroy(): void {
    this.destroySubject$.next();
    this.destroySubject$.complete();
  }
}
