import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { takeUntil } from 'rxjs/operators';
import {
  Product,
  Type,
  Line,
  Brand,
  Condition,
  Category
} from '../../models/index.models';
import { Router, ActivatedRoute } from '@angular/router';
import { PNotifyService, ErrorService, EntityService, ModalUploadService, ProductService } from '../../services/service.index';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { RECORDS_LIMIT, APP_NAME } from '../../config/config';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit, OnDestroy {
  destroySubject$: Subject<void> = new Subject();
  records: Product[] = [];
  model: Product = new Product(null, null, null, null, null, null, null, null, null, null, null);
  pnotify = this._pnotify.getPNotify();
  form: FormGroup;
  from = 0;
  totalRecords: Number = 0;
  value = '';
  table = 'product';
  formPrice: FormGroup;
  brands: Brand[];
  lines: Line[];
  types: Type[];
  categories: Category[];
  conditions: Condition[];

  constructor(
    private _pnotify: PNotifyService,
    private _ModelService: EntityService,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    private _ProductService: ProductService,
    private _errorS: ErrorService,
    public _modalUploadService: ModalUploadService,
    private titleService: Title
  ) {
    this.titleService.setTitle(APP_NAME + 'Producto');
    this._ModelService.getAll('brand')
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((data: any) => {
        this.brands = data.data;
      });

    this._ModelService.getAll('line')
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((data: any) => {
        this.lines = data.data;
      });

    this._ModelService.getAll('condition')
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((data: any) => {
        this.conditions = data.data;
      });

    this._ModelService.getAll('category')
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((data: any) => {
        this.categories = data.data;
      });

    this._ModelService.getAll('type')
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((data: any) => {
        this.types = data.data;
      });

    activatedRoute.params
      .pipe(takeUntil(this.destroySubject$))
      .subscribe(params => {
        this.form = new FormGroup({
          sku: new FormControl(null, [Validators.required]),
          barcode: new FormControl(null, [Validators.required]),
          name: new FormControl(null, [Validators.required]),
          type: new FormControl(null, [Validators.required]),
          line: new FormControl(null, [Validators.required]),
          price: new FormControl(null, [Validators.required]),
          brand: new FormControl(null, [Validators.required]),
          condition: new FormControl(null, [Validators.required]),
          code: new FormControl(null),
          sticker_sell: new FormControl(null),
          sticker_color: new FormControl(null),
          status: new FormControl(true),
          stock: new FormControl(0),
          category: new FormControl(null),
          date: new FormControl(),
          img_url: new FormControl(),
          img: new FormControl(),
          __v: new FormControl(),
        });
        if (typeof params['id'] !== 'undefined') {
          this.form.addControl('_id', new FormControl());
          this.getById(params['id']);
        }
      });
  }
  ngOnInit() {
    this.getAll();
  }

  getAll() {
    this._ModelService.getAll(this.table, this.from, this.value, RECORDS_LIMIT, 3)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((data: any) => {
        this.totalRecords = data.total;
        this.records = data.data;
      });
  }

  getById(id: string) {
    this._ModelService.getById(this.table, id)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((data: any) => {
        if (!data.sticker_sell) {
          data['sticker_sell'] = '';
        }
        if (!data.sticker_color) {
          data['sticker_color'] = '';
        }
        if (!data.name) {
          data['name'] = '';
        }
        this.model = data;
        this.form.setValue(this.model);
      }, (err) => {
        this.pnotify.error({
          title: 'Error',
          text: 'No se pudo recuperar la información, intentar mas tarde ' + err.error.err.message
        });
      });
  }


  save() {
    if (this.form.invalid) {
      this.pnotify.error({
        title: 'Error',
        text: 'Datos incorrectos favor revisar'
      });
      return;
    }
    this._ModelService.save(this.table, this.form.value)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe(resp => {
        if (resp.result) {
          this.pnotify.success({
            title: 'Conformación',
            text: 'Datos Guarados exitosamente'
          });
          this.getAll();
          this.router.navigate(['/' + this.table]);
          this.form.reset({
            sku: null,
            barcode: null,
            name: null,
            type: null,
            line: null,
            price: null,
            brand: null,
            condition: null,
            code: null,
            sticker_sell: null,
            sticker_color: null,
            status: true,
            stock: null,
            category: [],
            img: null,
            __v: null,
          });
        } else {
          this.pnotify.error({
            title: 'Error',
            text: 'No se pudo guardar la información, intentar mas tarde '
          });
        }
      }, (err) => {
        if (err.error.err.code === 11000) {
          this._errorS.showErrorDuplicate(err);
        } else {
          this._errorS.showErrors(err);
        }
      });

  }

  chageFrom(value) {
    let from = this.from + value;
    if (from >= this.totalRecords) {
      return;
    }

    if (from < 0) {
      return;
    }

    this.from += value;
    this.getAll();
  }


  active(id) {
    this._ModelService.active(this.table, id)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((resp: any) => {
        this.getAll();
        this.pnotify.success({
          title: 'Conformación',
          text: 'Datos Activado exitosamente'
        });
      }, (err) => {
        this.pnotify.error({
          title: 'Error',
          text: 'No se pudo guardar la información, intentar mas tarde ' + err
        });
      });
  }

  inactive(id) {
    this._ModelService.inactive(this.table, id)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((resp: any) => {
        this.pnotify.success({
          title: 'Conformación',
          text: 'Datos Inactivo exitosamente'
        });
        this.getAll();
      }, (err) => {
        this.pnotify.error({
          title: 'Error',
          text: 'No se pudo guardar la información, intentar mas tarde ' + err
        });
      });
  }

  search(value) {
    this.value = value;
    this.from = 0;
    this.totalRecords = 0;
    this.getAll();
  }

  cambiarFoto() {
    this._modalUploadService.mostrarModal('product', this.model._id);
  }
  ngOnDestroy(): void {
    this.destroySubject$.next();
    this.destroySubject$.complete();
  }
}
