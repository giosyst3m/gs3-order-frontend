import { Component, OnInit, OnDestroy } from '@angular/core';
import { Access, Role } from '../../models/index.models';
import { Router, ActivatedRoute } from '@angular/router';
import { PNotifyService, ErrorService, EntityService } from '../../services/service.index';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { RECORDS_LIMIT, APP_NAME } from '../../config/config';
import { Title } from '@angular/platform-browser';
import { Subject } from 'rxjs/Subject';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-access',
  templateUrl: './access.component.html',
  styleUrls: ['./access.component.css']
})
export class AccessComponent implements OnInit, OnDestroy {
  destroySubject$: Subject<void> = new Subject();
  records: Access[] = [];
  model: Access = new Access(null, null, null, null);
  pnotify = this._pnotify.getPNotify();
  form: FormGroup;
  from = 0;
  totalRecords: Number = 0;
  value = '';
  table = 'access';
  role: Role[];

  constructor(
    private _pnotify: PNotifyService,
    private _ModelService: EntityService,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    private _errorS: ErrorService,
    private titleService: Title
  ) {
    this.titleService.setTitle(APP_NAME + 'Accesos');
    this._ModelService.getAll('role')
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((resp: any) => {
        this.role = resp.data;
      });
    activatedRoute.params
      .pipe(takeUntil(this.destroySubject$))
      .subscribe(params => {
        this.form = new FormGroup({
          role: new FormControl(null, [Validators.required]),
          resource: new FormControl(null, [Validators.required]),
          action: new FormControl(null, [Validators.required]),
          attributes: new FormControl('*', [Validators.required]),
          status: new FormControl(true),
          __v: new FormControl(),
        });
        if (typeof params['id'] !== 'undefined') {
          this.form.addControl('_id', new FormControl());
          this.getById(params['id']);
        }
      });
  }
  ngOnInit() {
    this.getAll();
  }

  getAll() {
    this._ModelService.getAll(this.table, this.from, this.value, RECORDS_LIMIT, 3)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((data: any) => {
        this.totalRecords = data.total;
        this.records = data.data;
      }, (err) => {
        this.pnotify.error({
          title: err.err.title,
          text: err.message
        });
      });
  }

  getById(id: string) {
    this._ModelService.getById(this.table, id)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((data: any) => {
        if (!data.role) {
          data['role'] = '';
        }
        this.model = data;
        this.form.setValue(this.model);
      }, (err) => {
        this.pnotify.error({
          title: 'Error',
          text: 'No se pudo recuperar la información, intentar mas tarde ' + err.error.err.message
        });
      });
  }


  save() {
    if (this.form.invalid) {
      this.pnotify.error({
        title: 'Error',
        text: 'Datos incorrectos favor revisar'
      });
      return;
    }
    this._ModelService.save(this.table, this.form.value)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe(resp => {
        if (resp.result) {
          this.pnotify.success({
            title: 'Conformación',
            text: 'Datos Guarados exitosamente'
          });
          this.getAll();
          this.router.navigate(['/' + this.table]);
          this.form.reset({
            name: null,
            resource: null,
            action: null,
            attributes: '*',
            status: true,
            __v: null,
          });
        } else {
          this.pnotify.error({
            title: 'Error',
            text: 'No se pudo guardar la información, intentar mas tarde '
          });
        }
      }, (err) => {
        if (err.error.err.code === 11000) {
          this._errorS.showErrorDuplicate(err);
        } else {
          this._errorS.showErrors(err);
        }
      });

  }

  chageFrom(value) {
    let from = this.from + value;
    if (from >= this.totalRecords) {
      return;
    }

    if (from < 0) {
      return;
    }

    this.from += value;
    this.getAll();
  }


  active(id) {
    this._ModelService.active(this.table, id)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((resp: any) => {
        this.getAll();
        this.pnotify.success({
          title: 'Conformación',
          text: 'Datos Activado exitosamente'
        });
      }, (err) => {
        this.pnotify.error({
          title: 'Error',
          text: 'No se pudo guardar la información, intentar mas tarde ' + err
        });
      });
  }

  inactive(id) {
    this._ModelService.inactive(this.table, id)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((resp: any) => {
        this.pnotify.success({
          title: 'Conformación',
          text: 'Datos Inactivo exitosamente'
        });
        this.getAll();
      }, (err) => {
        this.pnotify.error({
          title: 'Error',
          text: 'No se pudo guardar la información, intentar mas tarde ' + err
        });
      });
  }

  search(value) {
    this.value = value;
    this.from = 0;
    this.totalRecords = 0;
    this.getAll();
  }
  ngOnDestroy(): void {
    this.destroySubject$.next();
    this.destroySubject$.complete();
  }
}
