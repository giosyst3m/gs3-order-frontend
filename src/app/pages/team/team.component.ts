import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { takeUntil } from 'rxjs/operators';
import { User, Company, Zone, Role, Client } from '../../models/index.models';
import { Router, ActivatedRoute } from '@angular/router';
import { PNotifyService, UserService, ErrorService, EntityService, ModalUploadService, ClientService } from '../../services/service.index';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { RECORDS_LIMIT, APP_NAME } from '../../config/config';
import { Title } from '@angular/platform-browser';
import swal from 'sweetalert2';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.css']
})
export class TeamComponent implements OnInit, OnDestroy {
  destroySubject$: Subject<void> = new Subject();
  records: User[] = [];
  model: User = new User(null, null, null, null, null, null);
  pnotify = this._pnotify.getPNotify();
  form: FormGroup;
  from = 0;
  totalRecords: Number = 0;
  value = '';
  table = 'user';
  role: Role[] = [];
  isClient = false;
  client = null;
  clientSelect: Client;
  zone: Zone[] = [];
  company: Company[] = [];
  create = true;

  constructor(
    private _pnotify: PNotifyService,
    private _ModelService: EntityService,
    private _UserService: UserService,
    private _ClientService: ClientService,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    private _errorS: ErrorService,
    public _modalUploadService: ModalUploadService,
    private titleService: Title

  ) {
    this.titleService.setTitle(APP_NAME + 'Equipo');
    this._ModelService.getAll('role', 0, '', RECORDS_LIMIT, 1)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((resp: any) => {
        this.role = resp.data;
      });
    this._ModelService.getAll('zone', 0, '', 20, 1)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((resp: any) => {
        this.zone = resp.data;
      });
    this._ModelService.getAll('company', 0, '', RECORDS_LIMIT, 1)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((resp: any) => {
        this.company = resp.data;
      });


    activatedRoute.params
      .pipe(takeUntil(this.destroySubject$))
      .subscribe(params => {
        this.form = new FormGroup({
          name: new FormControl(null, [Validators.required]),
          lastname: new FormControl(null, [Validators.required]),
          email: new FormControl(null, [Validators.required, Validators.email]),
          role: new FormControl(null, [Validators.required]),
          password: new FormControl(null),
          avatar: new FormControl('avatar'),
          client: new FormControl(null),
          zone: new FormControl(null, [Validators.required]),
          company: new FormControl(null, [Validators.required]),
          status: new FormControl(true),
          __v: new FormControl(),
        });

        if (typeof params['id'] !== 'undefined') {
          this.form.addControl('_id', new FormControl());
          this.getById(params['id']);
          this.create = false;
        } else {
          this.create = true;
        }
      });
  }
  ngOnInit() {
    this.getAll();
  }

  getAll() {
    this._ModelService.getAll(this.table, this.from, this.value, RECORDS_LIMIT, 3)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((data: any) => {
        this.totalRecords = data.total;
        this.records = data.data;
      });
  }

  getById(id: string) {
    this._ModelService.getById(this.table, id)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((data: any) => {
        this.client = null;
        this.isClient = false;
        if (!data.name) {
          data['name'] = '';
        }
        this.model = data;
        this.form.setValue(this.model);
        if (this.model.client != null) {
          this.client = this.model.client;
          this.isClient = true;
        }
      }, (err) => {
        this.pnotify.error({
          title: 'Error',
          text: 'No se pudo recuperar la información, intentar mas tarde ' + err.error.err.message
        });
      });
  }


  save() {
    this.form.controls['client'].setValue(this.client);
    if (this.form.invalid) {
      this.pnotify.error({
        title: 'Error',
        text: 'Datos incorrectos favor revisar'
      });
      return;
    }
    this._UserService.save(this.form.value)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe(resp => {
        if (resp.result && this.create) {
          swal({
            title: 'Confirmación Registro de Usuario',
            html: `Se ha registrado el usuario <b>${resp.data.name} ${resp.data.lastname}</b>
                             y se ha notificado por email <b>${ resp.data.email}</b> las indicaciones y contraseña para accedeer `,
            type: 'success'
          });
          this.router.navigate(['/team', resp.data._id]);
        } else {
          this.pnotify.success({
            title: 'Conformación',
            text: 'Datos Actualizados exitosamente'
          });
          this.getAll();
        }
      }, (err) => {
        if (err.error.err.code === 11000) {
          this._errorS.showErrorDuplicate(err);
        } else {
          this._errorS.showErrors(err);
        }
      });

  }

  chageFrom(value) {
    let from = this.from + value;
    if (from >= this.totalRecords) {
      return;
    }

    if (from < 0) {
      return;
    }

    this.from += value;
    this.getAll();
  }


  active(id) {
    this._ModelService.active(this.table, id)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((resp: any) => {
        this.getAll();
        this.pnotify.success({
          title: 'Conformación',
          text: 'Datos Activado exitosamente'
        });
      }, (err) => {
        this.pnotify.error({
          title: 'Error',
          text: 'No se pudo guardar la información, intentar mas tarde ' + err
        });
      });
  }

  inactive(id) {
    this._ModelService.inactive(this.table, id)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((resp: any) => {
        this.pnotify.success({
          title: 'Conformación',
          text: 'Datos Inactivo exitosamente'
        });
        this.getAll();
      }, (err) => {
        this.pnotify.error({
          title: 'Error',
          text: 'No se pudo guardar la información, intentar mas tarde ' + err
        });
      });
  }

  search(value) {
    this.value = value;
    this.from = 0;
    this.totalRecords = 0;
    this.getAll();
  }

  cambiarFoto() {
    this._modalUploadService.mostrarModal('user', this.model._id);
  }

  clientVerify(client: string) {
    this.client = client;
    this.pnotify.success({
      title: 'Conformación',
      text: 'Cliente Seleccionado'
    });
    // this.getClient();
  }

  roleSelect(role: string) {
    if (role == '_CLIENTE_') {
      this.isClient = true;
    } else {
      this.isClient = false;
      this.client = null;
    }
  }
  ngOnDestroy(): void {
    this.destroySubject$.next();
    this.destroySubject$.complete();
  }
}
