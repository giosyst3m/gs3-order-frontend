import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { takeUntil } from 'rxjs/operators';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Password } from '../../interface/interface-index';
import { UserService, PNotifyService } from '../../services/service.index';
import swal from 'sweetalert2';

@Component({
  selector: 'app-password',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.css']
})
export class PasswordComponent implements OnInit, OnDestroy {
  destroySubject$: Subject<void> = new Subject();
  @Input() UserID: string;
  form: FormGroup;
  model: Password;
  pnotify = this._pnotify.getPNotify();
  constructor(
    private _pnotify: PNotifyService,
    private _UserService: UserService
  ) {

    this.form = new FormGroup({
      password: new FormControl(null, [Validators.required, Validators.min(4)]),
      password2: new FormControl(null, [Validators.required, Validators.min(4)]),
      __v: new FormControl(),
    }, { validators: this.comparePassword('password', 'password2') });
  }

  comparePassword(pws1: string, pws2: string) {

    return (group: FormGroup) => {

      if (group.controls[pws1].value === group.controls[pws2].value) {
        return null;
      }
      return {
        comparePassword: true,
      };
    };

  }

  ngOnInit() {
  }

  /**
   * Save password
   */
  save() {

    swal({
      title: 'Confirmación de Cambio de contraseña',
      text: '¿Esta seguro en cambiar la contraseña del Usuario?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      confirmButtonText: 'Si, ¡Estoy seguro!',
      buttonsStyling: false,
    }).then((result) => {
      if (result.value) {
        this._UserService.reseatPassword(this.UserID, this.form.value)
          .pipe(takeUntil(this.destroySubject$))
          .subscribe((resp: any) => {
            swal({
              title: 'Confirmado',
              html: `Se cambiado la contraseña con éxito, el usuario recibirá un email <b>${resp.data.email}</b> con la nueva contraseña`,
              type: 'success',
            });
          }, (err) => {
            this.pnotify.error({
              title: 'Error',
              text: 'No se pudo recuperar la información, intentar mas tarde ' + err.error.err.message
            });
          });
      }
    });
  }
  ngOnDestroy(): void {
    this.destroySubject$.next();
    this.destroySubject$.complete();
  }

}
