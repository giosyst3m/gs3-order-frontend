import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { takeUntil } from 'rxjs/operators';
import { Document, Detail, Tax, Delivery, Discount, Inventory } from '../../models/index.models';
import { DocumentService, PNotifyService, CompanyService, InventoryService, UserService } from '../../services/service.index';
import { ActivatedRoute } from '@angular/router';
import { EntityService, CartService } from '../../services/service.index';
import { Router } from '@angular/router';
import { APP_NAME } from '../../config/config';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-document',
  templateUrl: './document.component.html',
  styleUrls: ['./document.component.css']
})
export class DocumentComponent implements OnInit, OnDestroy {
  destroySubject$: Subject<void> = new Subject();
  document: Document;
  dtTrigger: Subject<any> = new Subject();
  details: Detail[];
  taxes: Tax[];
  deliveries: Delivery[];
  discounts: Discount;
  id: string;
  totals: any;
  des: Discount[];
  totalRows: number;
  pnotify = this._pnotify.getPNotify();
  from = 0;
  lodaing = true;
  inventory: Inventory;
  role = [];
  roleProduct = [];
  user: any;
  
  constructor(
    public activatedRoute: ActivatedRoute,
    private router: Router,
    private _DocumentS: DocumentService,
    private _EntityS: EntityService,
    private _pnotify: PNotifyService,
    private _ConpmayS: CompanyService,
    private _IventoryS: InventoryService,
    private _CartS: CartService,
    private _UserService: UserService,
    private titleService: Title
  ) {
    this.titleService.setTitle(APP_NAME + 'Pedidos');
    activatedRoute.params
      .pipe(takeUntil(this.destroySubject$))
      .subscribe(params => {
        this.id = params['id'];
        this.getDocument();
      });


  }

  sendData() {
    this._CartS.getAll(this.id)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((resp: any) => {
        if (resp.total > 0) {
          this.details = resp;
          this.details['document'] = this.id;
          this._CartS.sendData(this.details);
          this._DocumentS.updateCart(resp.data[0].document.number);
        } else {
          this._DocumentS.updateCart(this.document.number);
        }
      });
  }

  getroles(role) {
    this.role = [];
    role.forEach(element => {
      this.role.push(element.name);
    });
  }
  getrolesAddProduct(role) {
    this.roleProduct = [];
    role.forEach(element => {
      this.roleProduct.push(element.name);
    });
  }

  getDocument() {
    this._DocumentS.getById(this.id)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((resp: any) => {
        this.getroles(resp.data.status.role);
        this.getrolesAddProduct(resp.data.status.addProduct);
        this.document = resp['data'];
        this.taxes = resp['data']['tax'];
        this.deliveries = resp['data']['delivery'];
        this.discounts = resp['data']['discount'];
        this.getTotals();
        this.getAll();
        this.sendData();
      });
  }

  getTotals() {
    this._DocumentS.getTotalsById(this.id)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((resp: any) => {
        this.totals = resp['data'];
      });
  }

  getAll() {
    this.lodaing = true;
    this.getDetail();
  }

  getDetail() {
    this._DocumentS.getDetail(this.id, this.from)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((resp: any) => {
        this.details = resp.data;
        this.totalRows = resp.total;
        this.lodaing = false;
      });
  }

  ngOnInit() {
    this.user = this._UserService.getUser();
    // send message to subscribers via observable subject
    this._CartS.sendData(['Message from Child One Component!']);
    this._EntityS.getAll('discount')
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((resp) => {
        this.des = resp.data;
      });
  }

  changeDiscount(value) {
    if (value) {
      this.document.discount = value;
      this._EntityS.save('document', this.document)
        .pipe(takeUntil(this.destroySubject$))
        .subscribe((resp) => {
          this.getDocument();
        });
    }
  }

  chageQuantity(id, qty) {
    this._EntityS.save('detail', { _id: id, quantity: qty })
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((resp) => {
        this.getTotals();
        this.getDetail();
      });
  }

  deleteDeatil(id, data) {
    this._EntityS.delete('detail', id)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((resp) => {
        this.getTotals();
        this.getAll();
        this.addLog(data);
      });
  }

  addLog(data){
    let mlog = {
      code: '200',
      entity: 'document',
      id: data.document._id,
      message: `<b>Pedido Nro.:</b> ${ data.document.number }, Se eliminó el Producto<br>
        <b>Nombre:</b> ${ data.product.name }<br>
        <b>REF:</b> ${ data.product.sku }<br>
        <b>Código Barra:</b> ${ data.product.barcode }<br>
        <b>Cantidad:</b> <span class="text-danger">${ data.quantity }</span>`,
        type:'Delete | Product @ Order'
    }
    
    this._EntityS.save('log',mlog)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((resp) => {
        this.pnotify.success({
          title: 'Registro.',
          text: 'Se elimino el registro'
        });  
      });

  }

  chageFrom(value) {
    const from = this.from + value;
    if (from >= this.totalRows) {
      return;
    }

    if (from < 0) {
      return;
    }

    this.from += value;
    this.getAll();
  }

  deleteDeatilInZero() {
    const doc = document.getElementsByClassName('_detail');
    const dataDelete: string[] = [];
    for (let index = 0; index < doc.length; index++) {

      if (Number(doc[index]['value']) === 0) {
        const id = doc[index]['id'].split('-');
        dataDelete.push(id[1]);
      }

    }

    if (dataDelete.length > 0) {
      this._DocumentS.deleteDeatilInZero(dataDelete)
        .pipe(takeUntil(this.destroySubject$))
        .subscribe((resp) => {
          this.getDetail();
          this.pnotify.info({
            title: 'Registros',
            text: 'Se Eliminaron registros con 0'
          });
        });
    }
  }

  deleteDetailNoInventory() {
    this._DocumentS.deleteDetailNoInventory(this.document._id)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((resp) => {
        if (resp > 0) {
          this.getDetail();
          this.pnotify.info({
            title: 'Registros',
            text: `Se Eliminaron ${resp} registros`
          });
        }
      });
  }

  nextStep(status) {
    this._DocumentS.save({ _id: this.document._id, status: status, total: this.totals.total })
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((resp) => {
        if (this.document.status['deleteDetailInZero']) {
          this.deleteDeatilInZero();
        }
        if (this.document.status['deleteDetailNoInventory']) {
          this.deleteDetailNoInventory();
        }
        if (resp.status === 200) {
          this.pnotify.success({
            title: 'Registro.',
            text: 'Se actualizó El Pedido'
          });
          this.getDocument();
        } else {
          this.pnotify.error({
            title: 'Registro.',
            text: 'No se pudo actualizar el Pedido'
          });
        }
      }, (err) => {
        this.pnotify.error({
          title: 'Error al intentar de actualizar el Pedido',
          text: err.error.error.message
        });
      });
  }

  chageQuantitySotck(id: string, qty: number, req: number, detail: string, stock: number) {

    this.inventory = {
      product: id,
      quantity: qty,
      required: req,
      company: this._ConpmayS.getCompanySelect(),
      origin: this.router.url,
      document: this.document._id,
      type: false,
      detail: detail,
      status: this.document.status['_id']
    };
    this._IventoryS.save(this.inventory)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((resp) => {
        this.pnotify.success({
          title: 'Actualización',
          text: 'Se asignó las unidades al Producto'
        });
        this.getDetail();
      }, (err) => {
        this.pnotify.error({
          title: 'Error',
          text: err.error.err.message
        });
      });
  }

  ngOnDestroy(): void {
    this.destroySubject$.next();
    this.destroySubject$.complete();
  }
}
