import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { takeUntil } from 'rxjs/operators';
import { Delivery } from '../../models/index.models';
import { PNotifyService, DeliveryService } from '../../services/service.index';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { APP_NAME } from '../../config/config';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-delivey',
  templateUrl: './delivey.component.html',
  styleUrls: ['./delivey.component.css']
})
export class DeliveyComponent implements OnInit, OnDestroy {
  destroySubject$: Subject<void> = new Subject();
  delivery: Delivery = new Delivery(null, null);
  pnotify = this._pnotify.getPNotify();
  form: FormGroup;

  @Input() document: string;
  @Output() getTotal: EventEmitter<any>;

  constructor(
    private _pnotify: PNotifyService,
    private _DeliveryService: DeliveryService,
    private titleService: Title
  ) {
    this.titleService.setTitle(APP_NAME + 'Entrega');
    this.getTotal = new EventEmitter();

    this.form = new FormGroup({
      company: new FormControl('', Validators.required),
      price: new FormControl(0, [Validators.required]),
    });
  }

  ngOnInit() {
  }

  save() {
    if (this.form.invalid) {
      return;
    }

    this._DeliveryService.save(this.document, this.form.value)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((resp) => {
        this.pnotify.success({
          title: 'Confirmación',
          text: 'Se Agergó el Costo de Envío al Pedido '
        });
        this.form.reset({
          _id: null,
          name: null,
          status: true,
          __v: null,
        });
        this.getTotal.emit(true);
      }, (err) => {
        this.pnotify.error({
          title: 'Error',
          text: err
        });
      });
  }
  ngOnDestroy(): void {
    this.destroySubject$.next();
    this.destroySubject$.complete();
  }
}
