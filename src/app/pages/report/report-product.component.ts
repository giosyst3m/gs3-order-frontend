import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { takeUntil } from 'rxjs/operators';
import { ReportService, PNotifyService, CompanyService } from '../../services/service.index';
import { APP_NAME } from '../../config/config';
import { Title } from '@angular/platform-browser';
import { Line } from '../../models/index.models';
import { EntityService } from '../../services/service.index';

@Component({
  selector: 'app-report-product',
  templateUrl: './report-product.component.html',
  styleUrls: ['./report-product.component.css']
})
export class ReportProductComponent implements OnInit, OnDestroy {
  destroySubject$: Subject<void> = new Subject();
  loading_excel = false;
  loading_pdf = false;
  urlPDF = '';
  email = 'no-email';
  pnotify = this._pnotify.getPNotify();
  lines: Line[] = [];
  LinesSelected: string[] = [];

  columns: any[] = [
    {
      data: ':id',
      title: 'UID'
    },
    {
      data: 'name',
      title: 'NAME'
    },

  ];

  selectedColumns: any = [
    {
      data: '_id',
      title: 'UID'
    },
    {
      data: 'name',
      title: 'NAME'
    },

  ];

  onColumnSelect(selected_column) {
    this.selectedColumns = selected_column;
  }


  constructor(
    private _reportService: ReportService,
    private _pnotify: PNotifyService,
    private titleService: Title,
    private _CompanyService: CompanyService,
    private _EntityService: EntityService
  ) {
    this.titleService.setTitle(APP_NAME + 'Reportes');
    this._EntityService.getAll('line', 0, '', 20)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((resp: any) => {
        this.lines = resp.data;
      });
  }

  exportProductExcel() {
    if (this.LinesSelected.length < 1) {
      this.pnotify.error({
        title: 'warning',
        text: `Debes seleccionar al menos una línea`
      });
      return;
    }
    this.loading_excel = true;
    this._reportService.getProductToExcel(this.LinesSelected)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((resp: any) => {
        this._reportService.exportAsExcelFile(resp.data, 'catalogo');
        this.loading_excel = false;
      });
  }

  sendEmail(email: string) {
    this.email = email;
  }


  exportPDF(img: boolean = true) {
    if (this.LinesSelected.length < 1) {
      this.pnotify.error({
        title: 'warning',
        text: `Debes seleccionar al menos una línea`
      });
      return;
    }
    this.loading_pdf = true;
    this.urlPDF = '';

    if (!this.validateEmail(this.email) && this.email !== 'no-email') {
      this.pnotify.error({
        title: 'Error',
        text: `Correo Electrónico no Valido ${this.email}, favor revisar`
      });
    } else {
      this._reportService.getProductToPDF(this.email, this._CompanyService.getCompanySelect(), this.LinesSelected, img)
        .pipe(takeUntil(this.destroySubject$))
        .subscribe((resp: any) => {
          this.loading_pdf = false;
          this.email = 'no-email';
          this.urlPDF = resp.url;
        });
    }
  }

  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  ngOnInit() {

  }
  ngOnDestroy(): void {
    this.destroySubject$.next();
    this.destroySubject$.complete();
  }
}
