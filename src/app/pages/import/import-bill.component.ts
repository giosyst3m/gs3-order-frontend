import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-import-bill',
  templateUrl: './import-bill.component.html',
  styleUrls: ['./import-bill.component.css']
})
export class ImportBillComponent implements OnInit {
  @Input() Bills: any = [];
  constructor() { }

  ngOnInit() {
  }

}
