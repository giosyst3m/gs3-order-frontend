import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { takeUntil } from 'rxjs/operators';
import { EntityService, ZoneService, PNotifyService, DocumentService } from './../../services/service.index';
import { SubirArchivoService } from '../../services/shared/subir-archivo.service';
import { ActivatedRoute } from '@angular/router';
import { Zone } from '../../models/index.models';


@Component({
  selector: 'app-import',
  templateUrl: './import.component.html',
  styleUrls: ['./import.component.css']
})
export class ImportComponent implements OnInit, OnDestroy {
  destroySubject$: Subject<void> = new Subject();
  imagenSubir: File;
  imagenTemp: string;
  type: string;
  zones: Zone[];
  pnotify = this._pnotify.getPNotify();
  clients: any[] = [];
  clientsShow = false;
  products: any[] = [];
  productShow = false;
  bill: any[] = [];
  billShow = false;
  process = false;
  fileUrl = '#';
  upload = true;
  documentos = [];
  documentsTotal = 0;

  constructor(
    public _subirArchivoService: SubirArchivoService,
    public activatedRoute: ActivatedRoute,
    private _EntityService: EntityService,
    private _ZoneService: ZoneService,
    private _pnotify: PNotifyService,
    private _documentService: DocumentService
  ) {
    activatedRoute.params
      .pipe(takeUntil(this.destroySubject$))
      .subscribe(params => {
        this.type = params['type'];
        if (this.type === 'product') {
          this._documentService.getByStatus(['3', '4'])
            .pipe(takeUntil(this.destroySubject$))
            .subscribe((resp: any) => {
              this.documentos = resp.data;
              this.documentsTotal = resp.total;
            });
        } else {
          this.documentos = [];
          this.documentsTotal = 0;
        }
      });

    this._EntityService.getAll('zone')
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((resp: any) => {
        this.zones = resp.data;
      });
  }

  ngOnInit() {
  }

  selectFile(archivo: File) {

    if (!archivo) {
      this.imagenSubir = null;
      console.log('Seleccionar un archivo');
      return;
    }

    // if ( archivo.type.indexOf('image') < 0 ) {
    //   console.log('Sólo imágenes', 'El archivo seleccionado no es una imagen', 'error');
    //   this.imagenSubir = null;
    //   return;
    // }

    this.imagenSubir = archivo;

    let reader = new FileReader();
    let urlImagenTemp = reader.readAsDataURL(archivo);

    reader.onloadend = () => this.imagenTemp = reader.result;
    this.upload = true;
    this.clientsShow = false;
    this.productShow = false;
    this.billShow = false;
  }

  subirImagen() {
    let zone = null;
    if (this.type === 'client') {
      zone = this._ZoneService.getZoneSelect();
      if (!zone) {
        this.pnotify.error({
          title: 'Error',
          text: 'Debe Seleccionar una Zona'
        });
        return;
      }
    }

    if (!this.imagenSubir) {
      this.pnotify.error({
        title: 'Error',
        text: 'Debe Seleccionar un Archivo'
      });
      return;
    }
    this.process = true;
    this.clients = [];
    this.products = [];
    this.bill = [];
    this._subirArchivoService.uploadFileExel(this.imagenSubir, this.type, zone)
      .then((resp: any) => {
        // this.upload = false;
        this.fileUrl = resp.cloudinary.secure_url;
        this.process = false;
        if (this.type === 'client') {
          this.clientsShow = true;
          this.clients = resp.data;
          this.pnotify.success({
            title: 'Procesos de Finalizado',
            text: 'Clientes importados'
          });
        } else if (this.type === 'product') {
          this.productShow = true;
          this.products = resp.data;
          this.pnotify.success({
            title: 'Procesos de Finalizado',
            text: 'Productos importados'
          });
        } else if (this.type === 'bill') {
          this.billShow = true;
          this.bill = resp.data;
          this.pnotify.success({
            title: 'Procesos de Finalizado',
            text: 'Facturas importadas'
          });
        }

      })
      .catch(err => {
        this.pnotify.error({
          title: 'Se presento el siguiente error',
          text: err
        });
      });

  }
  ngOnDestroy(): void {
    this.destroySubject$.next();
    this.destroySubject$.complete();
  }
}
