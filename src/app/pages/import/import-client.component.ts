import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-import-client',
  templateUrl: './import-client.component.html',
  styleUrls: ['./import-client.component.css']
})
export class ImportClientComponent implements OnInit {
  @Input() Clients: any = [];

  constructor() { }

  ngOnInit() {
  }

}
