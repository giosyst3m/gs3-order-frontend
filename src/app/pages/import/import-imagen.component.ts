import { PNotifyService } from './../../services/service.index';
import { Component, OnInit } from '@angular/core';
import { SubirArchivoService } from '../../services/shared/subir-archivo.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-import-imagen',
  templateUrl: './import-imagen.component.html',
  styleUrls: ['./import-imagen.component.css']
})
export class ImportImagenComponent implements OnInit {

  imagenSubir: File[];
  imagenTemp: string;
  upload = false;
  pnotify = this._pnotify.getPNotify();
  products: any[] = [];
  process = false;
  result = [];

  constructor(
    public _subirArchivoService: SubirArchivoService,
    public activatedRoute: ActivatedRoute,
    private _pnotify: PNotifyService
  ) {

  }

  ngOnInit() {
  }

  selectFile(archivo: Array<File>) {
    if (!archivo) {
      this.imagenSubir = null;
      console.log('Seleccionar un archivo');
      return;
    }

    this.imagenSubir = archivo;
    if (this.imagenSubir.length > 100) {
      this.pnotify.alert({
        title: 'Advertencia',
        text: 'Ha seleccionado ' + this.imagenSubir.length + ' Se recomienda hasta 100 por subida.'
      });
    }
    this.upload = true;
  }

  subirImagen() {

    this.result = [];
    if (!this.imagenSubir) {
      this.pnotify.error({
        title: 'Error',
        text: 'Debe Seleccionar un Archivo'
      });
      return;
    }
    this.process = true;

    this.products = [];
    this._subirArchivoService.upalodImagenProduct(this.imagenSubir)
      .then((resp: any) => {
        this.result = resp.data;
        this.process = false;
        this.products = resp.data;
        this.pnotify.success({
          title: 'Procesos de Finalizado',
          text: 'Images Productos importados'
        });


      })
      .catch(err => {
        this.pnotify.error({
          title: 'Se presento el siguiente error',
          text: err
        });
      });

  }

}
