import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-import-product',
  templateUrl: './import-product.component.html',
  styleUrls: ['./import-product.component.css']
})
export class ImportProductComponent implements OnInit {
  @Input() Products: any = [];
  constructor() { }

  ngOnInit() {
  }

}
