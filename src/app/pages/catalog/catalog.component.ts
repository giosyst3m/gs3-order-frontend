import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { takeUntil } from 'rxjs/operators';
import {
  ProductService,
  EntityService,
  PNotifyService,
  DocumentService,
  CompanyService,
  TaxService,
  ErrorService,
  CartService,
  UserService
} from '../../services/service.index';
import {
  Product,
  Brand,
  Line,
  Category,
  Type,
  Document,
  Detail,
  User
} from '../../models/index.models';
import { APP_NAME } from '../../config/config';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.css']
})
export class CatalogComponent implements OnInit, OnDestroy {
  destroySubject$: Subject<void> = new Subject();
  products: Product[];
  brands: Brand[];
  brand: string;
  lines: Line[];
  line: string;
  types: Type[];
  categories: Category[];
  category: string;
  loading = true;
  keywords: string;
  from = 0;
  totalRecords: Number = 0;
  pnotify = this._pnotify.getPNotify();
  documents: Document[];
  document: Document;
  taxs: string[];
  detail: Detail;
  details: Detail[];
  User: User;

  constructor(
    private _PS: ProductService,
    private _ES: EntityService,
    private _pnotify: PNotifyService,
    private _DS: DocumentService,
    private _CPS: CompanyService,
    private _TXS: TaxService,
    private _error: ErrorService,
    private _CartS: CartService,
    private titleService: Title,
    private _UserService: UserService
  ) {
    this.titleService.setTitle(APP_NAME + 'Catálogo Gnerral');
    if (this._DS.getDocumentActive() > 0) {
      this._DS.getByNumber(this._DS.getDocumentActive())
        .pipe(takeUntil(this.destroySubject$))
        .subscribe((resp: any) => {
          this.document = resp.data;
          this.sendData();
        });
    }


  }

  changeBrand(value) {
    this.brand = value;
    this.from = 0;
    this.getAll();
  }

  changeLine(value) {
    this.line = value;
    this.from = 0;
    this.getAll();
  }

  changeCategory(value) {
    this.category = value;
    this.from = 0;
    this.getAll();
  }

  chageFrom(value) {
    let from = this.from + value;
    if (from >= this.totalRecords) {
      return;
    }

    if (from < 0) {
      return;
    }

    this.from += value;
    this.getAll();
  }

  getAll() {
    this.loading = true;
    this._PS.getAll(this.from, this.keywords, this.brand, this.line, this.category)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((data: any) => {
        this.products = data.data;
        this.totalRecords = data.total;
        this.loading = false;
      });

  }

  sendData() {
    this._CartS.getAll(this.document._id)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((resp: any[]) => {
        this.details = resp;
        this.details['document'] = this.document._id;
        this._CartS.sendData(this.details);
      });
  }

  clearData() {
    // clear message
    this._CartS.clearData();
  }

  ngOnInit() {
    this.User = this._UserService.getUser();
    if (this.User.client) {
      this.clientVerify(this.User.client);
    }
    // send message to subscribers via observable subject
    this._CartS.sendData(['Message from Child One Component!']);

    this.getAll();
    this.getTaxs();

    this._ES.getAll('brand', 0, '', 50)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((data: any) => {
        this.brands = data.data;
      });

    this._ES.getAll('line')
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((data: any) => {
        this.lines = data.data;
      });

    this._ES.getAll('category')
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((data: any) => {
        this.categories = data.data;
      });
  }

  searching(keywords: string) {
    this.keywords = keywords;
    this.getAll();
  }

  clientVerify(client: string) {
    this.pnotify.info({
      title: 'Buscando...',
      text: 'Buscando pedidos del cliente'
    });
    this._DS.getByClient(client, this._CPS.getCompanySelect())
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((resp: any) => {
        this.document = resp.data;
        this.sendData();
        this.pnotify.alert({
          title: 'Resultado de la búsqueda',
          text: `Cliente ${resp.data.client.name} tiene el Pedido Nro.: ${resp.data.number} con el estado ${resp.data.status.name}`
        });
        this._DS.updateCart(resp.data.number);
      }, (err) => {
        this.pnotify.error({
          title: 'Consulta Finalizada',
          text: err.error.message
        });
        this.clearData();
        this.createOrderHeader(client);
      });
  }


  createOrderHeader(client) {

    this.document = {
      client: client,
      company: this._CPS.getCompanySelect(),
      tax: this.taxs
    };

    this._DS.createHeader(this.document, 'Order', 1)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((resp) => {
        this.document = resp.data;
        this.pnotify.success({
          title: 'Pedido Creado',
          text: `Se ha creado el Pedido Nro.: ${resp.data.number}`
        });
        this._DS.updateCart(resp.data.number);
      }, (err) => {
        this._error.showErrors(err);
      });
  }

  getTaxs() {
    this._TXS.getTaxCurrent()
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((data: string[]) => {
        this.taxs = data['data'];
      });
  }
  addCart(product) {
    let pr = document.getElementById(`qty-${product}`).attributes[4].ownerElement['value'];
    if (this._DS.getDocumentActive() <= 0) {
      this.pnotify.notice({
        title: 'Advertencia',
        text: 'Debe seleccionar un Cliente para Agregar el producto'
      });
    } else {
      this.detail = {
        document: this.document._id,
        product: product,
        quantity: pr,
        origin: 'Catalog: General'
      };

      this._DS.addCart(this.detail)
        .pipe(takeUntil(this.destroySubject$))
        .subscribe((data: Detail) => {
          if (data.status === 200) {
            this.pnotify.success({
              title: 'Carrito de compras',
              text: 'Se agregó el producto al carrito'
            });
            this.sendData();
          }
        }, (err) => {
          if (err.error.err.code === 11000) {
            this.pnotify.notice({
              title: 'Carrito',
              text: 'El producto ya esta agregado en el Carrito'
            });
          } else {
            this._error.showErrors(err);
          }

        });
    }
  }
  ngOnDestroy(): void {
    this.destroySubject$.next();
    this.destroySubject$.complete();
  }
}
