import { Component, OnInit, OnDestroy } from '@angular/core';
import {
  PNotifyService,
  DocumentService,
  CompanyService,
  CartService,
  TaxService,
  ErrorService,
  ProductService,
  EntityService
} from '../../services/service.index';
import { Document, Detail } from '../../models/index.models';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { APP_NAME } from '../../config/config';
import { Title } from '@angular/platform-browser';
import { Subject } from 'rxjs/Subject';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-catalog-simple',
  templateUrl: './catalog-simple.component.html',
  styleUrls: ['./catalog-simple.component.css']
})
export class CatalogSimpleComponent implements OnInit, OnDestroy {
  destroySubject$: Subject<void> = new Subject();
  pnotify = this._pnotify.getPNotify();
  documents: Document[];
  document: Document;
  detail: Detail;
  details: Detail[];
  details2: Detail[];
  taxs: string[];
  form: FormGroup;
  barcode: null;
  model: Detail = new Detail(null, null, null, null);
  formActive = false;
  lodaing = false;
  from = 0;
  totalRows: number;

  constructor(
    private _pnotify: PNotifyService,
    private _DS: DocumentService,
    private _CPS: CompanyService,
    private _CartS: CartService,
    private _TXS: TaxService,
    private _error: ErrorService,
    private _ProductS: ProductService,
    private _EntityS: EntityService,
    private titleService: Title
  ) {
    this.titleService.setTitle(APP_NAME + 'Catálogo Simple');
    this.form = new FormGroup({
      product: new FormControl(null, [Validators.required]),
      quantity: new FormControl(null, [Validators.required, Validators.min(1)]),
      origin: new FormControl('Catalog Simple', [Validators.required]),
    });
    if (this._DS.getDocumentActive() > 0) {
      this._DS.getByNumber(this._DS.getDocumentActive())
        .pipe(takeUntil(this.destroySubject$))
        .subscribe((resp: any) => {
          this.document = resp.data;
          this.formActive = true;
          this.sendData();
        });
    }
  }

  ngOnInit() {
  }

  clientVerify(client: string) {
    this.pnotify.info({
      title: 'Buscando...',
      text: 'Buscando pedidos del cliente'
    });
    this._DS.getByClient(client, this._CPS.getCompanySelect())
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((resp: any) => {
        this.document = resp.data;
        this.sendData();
        this.pnotify.alert({
          title: 'Resultado de la búsqueda',
          text: `Cliente ${resp.data.client.name} tiene el Pedido Nro.: ${resp.data.number} con el estado ${resp.data.status.name}`
        });
        this._DS.updateCart(resp.data.number);
        this.formActive = true;
      }, (err) => {
        this.pnotify.error({
          title: 'Consulta Finalizada',
          text: err.error.message
        });
        this.clearData();
        this.createOrderHeader(client);
      });
  }
  clearData() {
    // clear message
    this._CartS.clearData();
  }
  sendData() {
    this.lodaing = true;
    this._CartS.getAll(this.document._id)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((resp: any[]) => {
        this.details = resp;
        this.details['document'] = this.document._id;
        this._CartS.sendData(this.details);
        this.getDetail();
      });
  }

  createOrderHeader(client) {

    this.document = {
      client: client,
      company: this._CPS.getCompanySelect(),
      tax: this.taxs
    };

    this._DS.createHeader(this.document, 'Order', 1)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((resp) => {
        this.document = resp.data;
        this.pnotify.success({
          title: 'Pedido Creado',
          text: `Se ha creado el Pedido Nro.: ${resp.data.number}`
        });
        this._DS.updateCart(resp.data.number);
        this.formActive = true;
      }, (err) => {
        this._error.showErrors(err);
      });
  }

  getTaxs() {
    this._TXS.getTaxCurrent()
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((data: string[]) => {
        this.taxs = data['data'];
      });
  }

  save() {
    this.barcode = this.form.get('product').value;
    this.form.addControl('document', new FormControl(this.document._id, [Validators.required]));
    this._ProductS.getByBarcode(this.form.get('product').value)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((resp: any) => {
        this.form.setControl('product', new FormControl(resp.data._id, [Validators.required]));
        this.addCart();
      }, (error) => {
        this._error.showErroMessage(error);
      });
  }

  addCart() {
    this._DS.addCart(this.form.value)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((data: Detail) => {
        if (data.status === 200) {
          this.pnotify.success({
            title: 'Carrito de compras',
            text: 'Se agregó el producto al carrito'
          });
          this.sendData();
          this.form.reset({
            document: this.document._id,
            product: null,
            quantity: null,
            origin: 'Catalog Simple'
          });
        }
      }, (error) => {
        this.form.setControl('product', new FormControl(this.barcode, [Validators.required]));
        if (error.error.err.code === 11000) {
          this.pnotify.notice({
            title: 'Carrito',
            text: 'El producto ya esta agregado en el Carrito'
          });
        } else {
          this._error.showErrors(error);
        }
      });
  }

  chageQuantity(id, qty) {
    this._EntityS.save('detail', { _id: id, quantity: qty })
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((resp) => {
        this.sendData();
      });
  }

  deleteDeatil(id) {
    this._EntityS.delete('detail', id)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((resp) => {
        this.pnotify.success({
          title: 'Registro.',
          text: 'Se elimino el registro'
        });
        this.sendData();
      });
  }

  chageFrom(value) {
    const from = this.from + value;
    if (from >= this.totalRows) {
      return;
    }

    if (from < 0) {
      return;
    }

    this.from += value;
    this.getDetail();
  }

  getDetail() {
    this._DS.getDetail(this.document._id, this.from)
      .pipe(takeUntil(this.destroySubject$))
      .subscribe((resp: any) => {
        this.details2 = resp.data;
        this.totalRows = resp.total;
        this.lodaing = false;
      });
  }
  ngOnDestroy(): void {
    this.destroySubject$.next();
    this.destroySubject$.complete();
  }
}
