export interface UserLogin {
    avatar: string;
    client: string;
    email: string;
    name: string;
    role: string;
    token: string;
    _id: string
}
