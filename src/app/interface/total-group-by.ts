export interface TotalGroupBy {
    name: string;
    count: number;
    total: number;
    icon?: string;
    color?: string;
}
