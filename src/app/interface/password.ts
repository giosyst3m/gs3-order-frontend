export interface Password {
    password1: string;
    password2: string;
    _id: string;
}
